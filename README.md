# Update Niche

## Description

This repository contains the API to add new occurrence records to the database
of the SPECIES application without rebuilding the database. To use this API 
we must have the database builded with scripts in the repository 
[https://bitbucket.org/conabio_c3/speciesdbbuild] in a docker container or 
native way.

To configure the database connection, we must modify the file 
`./updateNiche/settings.py`, in the DATABASES dictionary changing current values
of the keys acording to the database configuration. 

## Dependencies instalation

For fast instalation, the dependencies are specified in the `requirements.txt`, 
we must have *pip* or *conda* installed. To install the dependencies with pip
we type the command  

```
pip install -r requirements.txt

```

or with conda

```
conda install --file requirements.txt

```

## Use

Django needs to create database migration tables, we don't want to create it
in the same database (niche), we must create a database specially for the 
django database migration, its name will be *django* and we use the 
`django_db.sql` file to create it, we only need to type: 

```
psql -h <HOST> -f django_db.sql

```

After execute the last command the django database are created, then to generate 
the migration tables we type 

```
python manage.py migrate

```

To run the server, we type the command

```
python manage.py runserver

```

when the server is runing, we can consume the API services.

To use the command line interface

```
python manage.py shell

```

## Run test

To run test, we only type the command

```
python manage.py test updatedb

```

If we dish the same test database every time we execute the above command we use 
the argument `--keepdb` in the next way

```
python manage.py test updatedb --keepdb

```

The `updatedb/test.py` file contains currently the tests, if we want to add a new
test we must write a method in the file whose name starts with "test_".

## Database 

The database configuration must be specified with the following environment variables:

```
	DBNAME1.- name of niche database
	DBHOST.- host's database
	DBUSER.- user's database
	DBPASS.- password of user database
	DBPORT.- port's database
	DBNAME2.- name of niche database for django migrations
	BROKERURL.- url for redis broker
``` 

## API Documentation

To see the interactive documentation we only enter to the url 
[http://<HOST>:8000/updatedb/docs/] after run the the server. We generate 
the automatic documentation with the django REST framework support.
