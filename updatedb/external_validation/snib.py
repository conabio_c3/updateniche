import json
import requests
import logging
from ..models import *

logger = logging.getLogger(__name__)

def create_ids_ascendente_string(L):
    """ Genera una parte del la url para consultar el arbol taxonomico completo
    de una especie
    """
    s = ""
    n = len(L)
    for i in range(n):
        if i < n - 1:
            s += "id:{0}+OR+".format(str(L[i]))
        else:
            s += "id:{0}".format(str(L[i]))
    return s

def get_cat_tax_con(data, cat_tax):
    """ Obtiene una categoria taxonomica especificada de la respuesta del
    servicio de arbol taxonomico de la CONABIO
    """
    aux_tax = ""
    if cat_tax == "reino":
        aux_tax = "categoria_taxonomica"
    else :
        aux_tax = "categoria_taxonomica"
    return next(obj for obj in data["response"]["docs"] if obj[aux_tax] == cat_tax)["nombre"]

def get_cat_tax_rl(data, cat_tax):
    """ Obtiene una categoria taxonomica especificada de la respuesta del
    servicio de arbol taxonomico del API de Red List
    """
    return data["result"][0][cat_tax] if len(data["result"]) > 0 else None

def get_tax_col(data, cat_tax):
    """
    Obtiene una categoria taxonomica especificada de la respuesta del servicio
    de arbol taxonomico del API de Cathalogue of Life
    """
    print()
    return next(obj for obj in data["results"][0]["classification"] if obj["rank"] == cat_tax)["name"]

def get_cadena_ascendentes(data):
	"""
	Obtiene la cadena de ascendentes de una especie para conseguir el arbol taxonomico de la conabio
	"""
	try:
		desired_obj = next(obj for obj in data["response"]["docs"] if obj["subcategoria_taxonomica"] == "especie")
	except:
		desired_obj = next(obj for obj in data["response"]["docs"] if obj["categoria_taxonomica"] == "especie")
	return desired_obj["ascendentes_oblig"]

def marca_validacion(data, is_validado):
    """ Marca para el registro de una especie en sp_snib si es validada en
    alguna de las APIs
    """
    sp_snib = None
    spsnib_data = {
                'reinovalido':data['reinovalido'],
                'phylumdivisionvalido':data['phylumdivisionvalido'],
                'clasevalida':data['clasevalida'],
                'ordenvalido':data['ordenvalido'],
                'familiavalida':data['familiavalida'],
                'generovalido':data['generovalido'],
                'especievalidabusqueda':data['especievalidabusqueda'],
                }
    try:
        sp_snib = SpSnib.objects.get(**spsnib_data)
    except Exception as e:
        logger.error(str(e))
        logger.warn("{0} no se pudo marcar".format(data['especievalidabusqueda']))
        return False
    sp_snib.validadoterceros = is_validado
    sp_snib.save()
    logger.info("{0} marcado correctamente {1}!".format(data['especievalidabusqueda'], is_validado))
    return True

def gbif_validation(in_specie, in_phylum, in_kingdom):
	"""
	Funcion para hacer validacion de especie en el API de GBIF
	"""
	insert = False
	phylum = None
	kingdom = None
	validator = ValidaTerceros.objects.get(nombre="gbif")
	url = validator.url.format(in_specie.replace(" ", "%20"))
	response = requests.get(url)
	data = response.json()
	try:
		phylum = data["phylum"].lower()
	except Exception as e:
		logger.error("No se obtuvo phylum")
	try:
		kingdom = data["kingdom"].lower()
	except Exception as e:
		logger.error("No se obtuvo reino")
	if data["matchType"] == "NONE":
		pass
	elif in_phylum.lower() == phylum and in_kingdom.lower() == kingdom:
		insert = True
		logger.info("{0} validado en GBIF API".format(in_specie))
	else:
		logger.warn("GBIF API:(" + str(phylum) + ", " + str(kingdom) + ")")
	return insert

def redlist_validation(in_specie, in_phylum, in_kingdom):
	"""
	Funcion para hacer validacion de especie en el catalogo de la Red Line
	"""
	insert = False
	phylum = None
	kingdom = None
	validator = ValidaTerceros.objects.get(nombre="red_list")
	with open('servicios_externos.json', 'r') as f:
		servicios_externos = json.load(f)
	token = servicios_externos["red_list_token"]
	url = validator.url.format(in_specie.replace(" ", "%20"), token)
	response = requests.get(url)
	data = response.json()
	try:
		phylum = get_cat_tax_rl(data, "phylum").lower()
	except Exception as e:
		logger.error("Red List API: No se obtuvo phylum")
	try:
		kingdom = get_cat_tax_rl(data, "kingdom").lower()
	except Exception as e:
		logger.error("Red List API: No se obtuvo reino")
	if len(data["result"]) == 0:
		pass
	elif in_phylum.lower() == phylum and in_kingdom.lower() == kingdom:
		insert = True
		logger.info("{0} validado en Red List API".format(in_specie))
	else:
		logger.warn("Red List API:(" + str(phylum) + ", " + str(kingdom) + ")")
	return insert

def c_of_life_validation(in_specie, in_phylum, in_kingdom):
	"""
	Funcion para hacer validacion de especie en el API cathalogue of life
	"""
	insert = False
	phylum = None
	kingdom = None
	validator = ValidaTerceros.objects.get(nombre="c_of_life")
	url = validator.url.format(in_specie.replace(" ", "+"))
	response = requests.get(url)
	data = response.json()
	try:
		phylum = get_tax_col(data, "Phylum").lower()
	except:
		logger.error("C. of Life API: No se obtuvo el phylum")
	try:
		kingdom = get_tax_col(data, "Kingdom").lower()
	except:
		logger.error("C. of Life API: No se obtuvo el kingdom")
	if data["error_message"] == "" and int(data["total_number_of_results"]) == 0:
		pass
	elif in_phylum.lower() == phylum and in_kingdom.lower() == kingdom:
		insert = True
		logger.info("{0} validado en C. of Life API".format(in_specie))
	else:
		logger.warn("C. of Life API:(" + str(phylum) + ", " + str(kingdom) + ")")
	return insert

def conabio_validation(in_specie, in_phylum, in_kingdom):
	"""
	Funcion para hacer validacion de especie en el catalogo de la CONABIO
	"""
	#   Validando especie
	insert = False
	phylum = None
	kingdom = None
	validator = ValidaTerceros.objects.get(nombre="conabio")
	url_val = validator.url.format("fq=subcategoria_taxonomica:especie&" , in_specie.replace(" ", "%20"))
	response_val = requests.get(url_val)
	data_val = response_val.json()
	#print(data_val)
	if len(data_val["response"]["docs"]) > 0:
		#   Obteniendo taxones
		ids_ascendentes_string = get_cadena_ascendentes(data_val)
	else:
		url_val = validator.url.format("fq=categoria_taxonomica:especie&" , in_specie.replace(" ", "%20"))
		response_val = requests.get(url_val)
		data_val = response_val.json()
		#print(data_val)
		if len(data_val["response"]["docs"]) > 0:
			#   Obteniendo taxones
			ids_ascendentes_string = get_cadena_ascendentes(data_val)
		else:
			logger.warn("No se obtivieron resultados para la especie")
			return False
	ids_ascendentes = sorted([int(s) for s in  ids_ascendentes_string.split(",") if s != ""], reverse=True)
	with open('servicios_externos.json', 'r') as f:
		servicios_externos = json.load(f)
	url_tax = servicios_externos["conabio_tax"].format(create_ids_ascendente_string(ids_ascendentes))
	response_tax = requests.get(url_tax)
	data_tax = response_tax.json()
	phylum_con = None
	kingdom_con = None
	try:
		phylum = get_cat_tax_con(data_tax, "phylum")
	except Exception as e:
		logger.error("CONABIO API: No se obtuvo phylum")
	try:
		kingdom = get_cat_tax_con(data_tax, "reino")
	except Exception as e:
		logger.error("CONABIO API: No se obtuvo reino")
	if len(data_tax["response"]["docs"]) == 0:
		pass
	elif phylum == in_phylum and kingdom == in_kingdom:
		insert = True
		logger.info("{0} validado en CONABIO".format(in_specie))
	else :
		logger.warn("CONABIO API:(" + str(phylum) + ", " + str(kingdom) + ")")
	return insert

def validate(especievalidabusqueda, phylumdivisionvalido, reinovalido):
	insert = False
	# Buscando especie en catalogo de la CONABIO
	#if False:
	if not insert:
		insert = conabio_validation(especievalidabusqueda, phylumdivisionvalido, reinovalido)
		if insert:
			return 1
	# Buscando especie en GBIF API
	if not insert:
		logger.info("{0} validado en GBIF API".format(especievalidabusqueda))
		insert = gbif_validation(especievalidabusqueda, phylumdivisionvalido, reinovalido)
		if insert:
			return 2
	# Buscando especie en Red List API
	if not insert:
		insert = redlist_validation(especievalidabusqueda, phylumdivisionvalido, reinovalido)
		if insert:
			return 3
		# Buscando especie en Catalogue of Life
	if not insert:
		insert = c_of_life_validation(especievalidabusqueda, phylumdivisionvalido, reinovalido)
		if insert:
			return 4
	# Especie no validada
	if not insert:
		logger.info("{0} no validado en APIs".format(especievalidabusqueda))
		return 0
