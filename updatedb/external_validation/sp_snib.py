import json
import requests
import logging
from ..models import *
from .snib import *

logger = logging.getLogger(__name__)

def has_synonyms(in_specie):
	"""
	Funcion para checar synonymos en el snib
	"""
	insert = False
	phylum = None
	kingdom = None
	validator = ValidaTerceros.objects.get(nombre="conabio")
	url_val = validator.url.format("fq=subcategoria_taxonomica:especie&" , in_specie.replace(" ", "%20"))
	response_val = requests.get(url_val)
	data_val = response_val.json()
	#print(data_val)
	if len(data_val["response"]["docs"]) > 0:
		ids_ascendentes_string = get_cadena_ascendentes(data_val)
	else:
		url_val = validator.url.format("fq=categoria_taxonomica:especie&" , in_specie.replace(" ", "%20"))
		response_val = requests.get(url_val)
		data_val = response_val.json()
		if len(data_val["response"]["docs"]) > 0:
			ids_ascendentes_string = get_cadena_ascendentes(data_val)
		else:
			logger.warn("No se obtivieron resultados para la especie")
			return []
	ids_ascendentes = sorted([int(s) for s in  ids_ascendentes_string.split(",") if s != ""], reverse=True)
	with open('servicios_externos.json', 'r') as f:
		servicios_externos = json.load(f)
	url_tax = servicios_externos["conabio_tax"].format(create_ids_ascendente_string(ids_ascendentes))
	response_tax = requests.get(url_tax)
	data_tax = response_tax.json()
	try:
		data_tax = data_tax['response']['docs']
		for obj in data_tax:
			if obj['categoria_taxonomica'] == 'especie':
				synonyms = obj['sinonimos']
	except Exception as e:
		logger.warn("No se obtuvieron resultados para {0}".format(in_specie))
		return []
	#print(synonyms)
	if not synonyms:
		synonyms = []
	return synonyms