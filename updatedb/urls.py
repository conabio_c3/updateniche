from django.urls import path
from . import views
from django.conf.urls import url
from rest_framework.documentation import include_docs_urls
from rest_framework.views import APIView
from rest_framework.documentation import get_docs_view
from rest_framework.documentation import get_schemajs_view
from rest_framework.schemas import get_schema_view

schema_view = get_schema_view(title="API Documentation Schema")

urlpatterns = [
    url(r'^docs/', include_docs_urls(title='Update Niche2', public=False)),
    url(r'^schema/', schema_view),
    url(r'^insertgeosnib/', views.GeoSnibView.as_view(), name='insertgeosnib'),
    url(r'^insertispsnib/', views.IspSnibView.as_view(), name='insertispsnib'),
    url(r'^insertspsnib/', views.SpSnibView.as_view(), name='insertspsnib'),
    url(r'^insertsnib/', views.SnibView.as_view(), name='insertsnib'),
    url(r'^multinsertsnib', views.MultiSnibView.as_view(), name='multinsertsnib')
]