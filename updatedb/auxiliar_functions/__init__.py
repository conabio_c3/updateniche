import json
from updatedb.models import *
from django.db.models import Q


def comprobar_servicio(namefile):
	"""
	Funcion para verificar si se insertaron correctamente los registros de snib dado el nombre del archivo
	"""
	count_snib=0
	count_spsnib=0
	count_ispsnib=0
	count_geosnib=0
	count_8g=0
	count_16g=0
	count_32g=0
	count_64g=0
	count_64_sp=0
	count_32_sp=0
	count_16_sp=0
	count_8_sp=0
	count_64_isp=0
	count_32_isp=0
	count_16_isp=0
	count_8_isp=0

	with open(namefile, 'r', encoding='utf-8') as f:
	    data = json.loads(f.read())

	data = data['data']
	for i in range(len(data)):
		data[i].pop('Unnamed: 0')
		data[i].pop('categoriainfraespecificavalida')

	snib_list = [Snib(**map) for map in data]

	for snib in snib_list:
		
		try:
			Snib.objects.get(idejemplar=snib.idejemplar)
			count_snib +=1
		except Snib.DoesNotExist:
			print("No se inserto snib {0}".format(snib.idejemplar))
			continue
		try:
			specie = SpSnib.objects.get(Q(especievalidabusqueda=snib.especievalidabusqueda) & 
										Q(phylumdivisionvalido=snib.phylumdivisionvalido) & 
										Q(reinovalido=snib.reinovalido))
			count_spsnib +=1
		except SpSnib.DoesNotExist:
			print("No se inserto sp_snib {0}".format(snib.especievalidabusqueda))

		try:
			intersection = GeoSnib.objects.get(longitud=snib.longitud,latitud=snib.latitud)
			count_geosnib +=1
		except GeoSnib.DoesNotExist:
			print("No se inserto geo_snib ({0}, {1})".format(snib.longitud, snib.latitud))

		grid8 = Grid8KmAoi.objects.get(gridid_8km=intersection.gridid_8km)

		if specie.spid in getattr(grid8, snib.reinovalido.lower()):
			count_8g +=1

		grid16 = Grid16KmAoi.objects.get(gridid_16km=intersection.gridid_16km)

		if specie.spid in getattr(grid16, snib.reinovalido.lower()):
			count_16g +=1

		grid32 = Grid32KmAoi.objects.get(gridid_32km=intersection.gridid_32km)

		if specie.spid in getattr(grid32, snib.reinovalido.lower()):
			count_32g +=1

		grid64 = Grid64KmAoi.objects.get(gridid_64km=intersection.gridid_64km)

		if specie.spid in getattr(grid64, snib.reinovalido.lower()):
			count_64g +=1

		if grid64.gridid_64km in specie.cells_64km:
				count_64_sp += 1
		if grid32.gridid_32km in specie.cells_32km:
			count_32_sp += 1
		if grid16.gridid_16km in specie.cells_16km:
			count_16_sp += 1
		if grid8.gridid_8km in specie.cells_8km:
			count_8_sp += 1

		try:
			ispecie = IspSnib.objects.get(Q(reinovalido=snib.reinovalido) & 
								Q(phylumdivisionvalido=snib.phylumdivisionvalido) & 
								Q(clasevalida=snib.clasevalida) & 
								Q(ordenvalido=snib.ordenvalido) & 
								Q(familiavalida=snib.familiavalida) & 
								Q(generovalido=snib.generovalido) & 
								Q(especievalidabusqueda=snib.especievalidabusqueda) & 
								Q(especievalida=snib.especievalida) & 
								Q(categoriainfraespecievalida=snib.categoriainfraespecievalida))
			count_ispsnib += 1
			if grid64.gridid_64km in ispecie.cells_64km:
				count_64_isp += 1
			if grid32.gridid_32km in ispecie.cells_32km:
				count_32_isp += 1
			if grid16.gridid_16km in ispecie.cells_16km:
				count_16_isp += 1
			if grid8.gridid_8km in ispecie.cells_8km:
				count_8_isp += 1
		except IspSnib.DoesNotExist:
			print("No se inserto isp_snib {0}".format(snib.especievalidabusqueda))

	print(count_snib, 
				  count_spsnib, 
				  count_ispsnib, 
				  count_geosnib, 
				  count_8g, 
				  count_16g, 
				  count_32g, 
				  count_64g,
				  count_64_sp,
				  count_32_sp,
				  count_16_sp,
				  count_8_sp,
				  count_64_isp,
				  count_32_isp,
				  count_16_isp,
				  count_8_isp)

	for count in (count_snib, 
				  count_spsnib, 
				  count_ispsnib, 
				  count_geosnib, 
				  count_8g, 
				  count_16g, 
				  count_32g, 
				  count_64g,
				  count_64_sp,
				  count_32_sp,
				  count_16_sp,
				  count_8_sp,
				  count_64_isp,
				  count_32_isp,
				  count_16_isp,
				  count_8_isp):
		if count != count_snib:
			return False
	return True