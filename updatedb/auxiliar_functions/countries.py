from updatedb.models.countries import *
from updatedb.models.grid import *
from updatedb.models.rasters import *
from django.contrib.gis.geos import GEOSGeometry, Polygon, MultiPolygon, Point, LineString
from django.core.serializers import serialize
from django.apps import apps
import rasterio
from rasterio.mask import mask
from osgeo import gdal
import numpy as np
import logging
import json
import os

logger = logging.getLogger(__name__)

def get_american_countries():
	try:
		american_countries = list(America.objects.all().values('gid', 'country').distinct())
		#american_countries = [obj['country'] for obj in american_countries if obj['country'] != None]
	except Exception as e:
		logger.error(str(e))
		return None
	return american_countries


def add_american_country(gid):
	try:
		selected_country = America.objects.get(gid=gid)
	except Exception as e:
		logger.error(str(e))
		return False
	try:
		existing_country = Aoi.objects.get(country=selected_country.country, fgid=selected_country.gid)
		logger.info("country {0} exists in aoi, it cannot be added".format(selected_country.country))
		return False 
	except Aoi.DoesNotExist:
		logger.info("country {0} doesn't exist in aoi, adding".format(selected_country.country))
	except Aoi.MultipleObjectsReturned:
		logger.info("country {0} exists in aoi, it cannot be added".format(selected_country.country))
		return False
	except Exception as e:
		logger.error(str(e))
		return False
	new_country = Aoi(fgid=selected_country.gid, country=selected_country.country, 
					  geom=selected_country.geom)
	new_country.save()
	if not generate_grid_64km(new_country.gid):
		logger.error("it couldn't generate the 64km grid of country {0}".format(new_country.country))
		return False
	return True

def generate_grid_64km(gid):
	try:
		selected_country = Aoi.objects.get(gid=gid)
	except Exception as e:
		logger.error(str(e))
		return False
	try:
		envelope = selected_country.geom.envelope
		new_rectangle = CotaAmerica(id_c=0, geom=MultiPolygon(envelope, srid=4326))
		new_rectangle.save()
		xmin, ymin, xmax, ymax = envelope.extent
		pmin = Point(xmin, ymin, srid=4326)
		pmax = Point(xmax, ymax, srid=4326)
	except Exception as e:
		logger.error(str(e))
		return False
	new_grid = Grid64Km.objects.filter(the_geom__intersects=selected_country.geom)
	N_cells = new_grid.count()
	for i in range(N_cells):
		curr = new_grid[i]
		new_cell = Grid64KmAoi(gcol=curr.gcol, grow=curr.grow, geom=curr.geom, the_geom=curr.the_geom,
							   small_geom=curr.small_geom, animalia=[], plantae=[], fungi=[], prokaryotae=[],
							   protoctista=[])
		new_cell.save()
		if not generate_grid_xxkm(32, new_cell):
			logger.error("it couldn't generate the 32km grid of country ")
			return False
	return True

def generate_grid_xxkm(xx, new_cell):
	try:
		xmin, ymin, xmax, ymax = new_cell.small_geom.extent
		p1 = Point(xmin, ymax)
		p2 = Point((xmax + xmin)/2, ymax)
		p3 = Point(xmax, ymax)
		p4 = Point(xmin, (ymax + ymin)/2)
		p5 = Point((xmax + xmin)/2, (ymax + ymin)/2)
		p6 = Point(xmin, (ymax + ymin)/2)
		p7 = Point(xmin, ymin)
		p8 = Point((xmax + xmin)/2, ymin)
		p9 = Point(xmax, ymin)
		#print(p1, p2, p3, p4, p5, p6, p7, p8, p9)
		polygon1 = Polygon([p1, p2, p5, p4, p1])
		polygon1_4326 = Polygon([p1, p2, p5, p4, p1], srid=4326)
		#print(polygon1_4326.envelope)
		polygon2 = Polygon([p2, p3, p6, p5, p2])
		polygon2_4326 = Polygon([p2, p3, p6, p5, p2], srid=4326)
		polygon3 = Polygon([p4, p5, p8, p7, p4])
		polygon3_4326 = Polygon([p4, p5, p8, p7, p4], srid=4326)
		polygon4 = Polygon([p5, p6, p9, p8, p5])
		polygon4_4326 = Polygon([p5, p6, p9, p8, p5], srid=4326)
		if xx == 32:
			cell1 = Grid32KmAoi(gcol=new_cell.gcol, grow=new_cell.grow, gridid_64km=new_cell.gridid_64km,
								geom=polygon1, the_geom=polygon1_4326, 
								small_geom=polygon1_4326.envelope, animalia=[], plantae=[], fungi=[], prokaryotae=[], protoctista=[])
			cell2 = Grid32KmAoi(gcol=new_cell.gcol, grow=new_cell.grow, gridid_64km=new_cell.gridid_64km,
								geom=polygon2, the_geom=polygon2_4326, 
								small_geom=polygon2_4326.envelope, animalia=[], plantae=[], fungi=[], prokaryotae=[], protoctista=[])
			cell3 = Grid32KmAoi(gcol=new_cell.gcol, grow=new_cell.grow, gridid_64km=new_cell.gridid_64km,
								geom=polygon3, the_geom=polygon3_4326, 
								small_geom=polygon3_4326.envelope, animalia=[], plantae=[], fungi=[], prokaryotae=[], protoctista=[])
			cell4 = Grid32KmAoi(gcol=new_cell.gcol, grow=new_cell.grow, gridid_64km=new_cell.gridid_64km,
								geom=polygon4, the_geom=polygon4_4326, 
								small_geom=polygon4_4326.envelope, animalia=[], plantae=[], fungi=[], prokaryotae=[], protoctista=[])
			cell1.save()
			cell2.save()
			cell3.save()
			cell4.save()
			generate_grid_xxkm(16, cell1)
			generate_grid_xxkm(16, cell2)
			generate_grid_xxkm(16, cell3)
			generate_grid_xxkm(16, cell4)
		elif xx == 16:
			cell1 = Grid16KmAoi(gcol=new_cell.gcol, grow=new_cell.grow, gridid_64km=new_cell.gridid_64km,
								gridid_32km=new_cell.gridid_32km, geom=polygon1, the_geom=polygon1_4326, 
								small_geom=polygon1_4326.envelope, animalia=[], plantae=[], fungi=[], prokaryotae=[], protoctista=[])
			cell2 = Grid16KmAoi(gcol=new_cell.gcol, grow=new_cell.grow, gridid_64km=new_cell.gridid_64km,
								gridid_32km=new_cell.gridid_32km, geom=polygon2, the_geom=polygon2_4326, 
								small_geom=polygon2_4326.envelope, animalia=[], plantae=[], fungi=[], prokaryotae=[], protoctista=[])
			cell3 = Grid16KmAoi(gcol=new_cell.gcol, grow=new_cell.grow, gridid_64km=new_cell.gridid_64km,
								gridid_32km=new_cell.gridid_32km, geom=polygon3, the_geom=polygon3_4326, 
								small_geom=polygon3_4326.envelope, animalia=[], plantae=[], fungi=[], prokaryotae=[], protoctista=[])
			cell4 = Grid16KmAoi(gcol=new_cell.gcol, grow=new_cell.grow, gridid_64km=new_cell.gridid_64km,
								gridid_32km=new_cell.gridid_32km, geom=polygon4, the_geom=polygon4_4326, 
								small_geom=polygon4_4326.envelope, animalia=[], plantae=[], fungi=[], prokaryotae=[], protoctista=[])	
			cell1.save()
			cell2.save()
			cell3.save()
			cell4.save()
			generate_grid_xxkm(8, cell1)
			generate_grid_xxkm(8, cell2)
			generate_grid_xxkm(8, cell3)
			generate_grid_xxkm(8, cell4)
		elif xx == 8:
			cell1 = Grid8KmAoi(gcol=new_cell.gcol, grow=new_cell.grow, gridid_64km=new_cell.gridid_64km,
								gridid_32km=new_cell.gridid_32km, gridid_16km=new_cell.gridid_16km, 
								geom=polygon1, the_geom=polygon1_4326, 
								small_geom=polygon1_4326.envelope, animalia=[], plantae=[], fungi=[], prokaryotae=[], protoctista=[])
			cell2 = Grid8KmAoi(gcol=new_cell.gcol, grow=new_cell.grow, gridid_64km=new_cell.gridid_64km,
								gridid_32km=new_cell.gridid_32km, gridid_16km=new_cell.gridid_16km,
								geom=polygon2, the_geom=polygon2_4326, 
								small_geom=polygon2_4326.envelope, animalia=[], plantae=[], fungi=[], prokaryotae=[], protoctista=[])
			cell3 = Grid8KmAoi(gcol=new_cell.gcol, grow=new_cell.grow, gridid_64km=new_cell.gridid_64km,
								gridid_32km=new_cell.gridid_32km, gridid_16km=new_cell.gridid_16km,
								geom=polygon3, the_geom=polygon3_4326, 
								small_geom=polygon3_4326.envelope, animalia=[], plantae=[], fungi=[], prokaryotae=[], protoctista=[])
			cell4 = Grid8KmAoi(gcol=new_cell.gcol, grow=new_cell.grow, gridid_64km=new_cell.gridid_64km,
								gridid_32km=new_cell.gridid_32km, gridid_16km=new_cell.gridid_16km,
								geom=polygon4, the_geom=polygon4_4326, 
								small_geom=polygon4_4326.envelope, animalia=[], plantae=[], fungi=[], prokaryotae=[], protoctista=[])	
			cell1.save()
			cell2.save()
			cell3.save()
			cell4.save()
	except Exception as e:
		logger.error(str(e))
		return False
	return True

def fill_layers_new_country(country):
	# ToDo: get all layers
	for layer in all_layers:
		try:
			fill_layer_new_country(country, layer)
		except:
			logger.error("an error has ocurred during the integration of layer {0} of country {1}".format(layer, country))
			return False
		logger.info("layer {0} integrated for country {1}!!".format(layer, country))		
	return True

def fill_layer_new_country(country, layer):
	resolutions = [64, 32, 16, 8]
	for xx in resolutions:
		try:
			fill_layer_gridxxkm_new_country(country, layer, xx)
		except:	
			logger.error("an error has ocurred during the integration of layer {0} in resolution {1} of country {2}".format(layer, xx, country))
			return False
		logger.info("layer {0} integrated in resolution {1} for country {2}!".format(layer, xx, country))
	return True

def fill_layer_gridxxkm_new_country(country, layer, xx):
	layer_name = ''
	layer_name = get_raster_name(layer)
	if layer_name == '':
		logger.warn("It can't get the discretized raster {0}".format(layer))
		return False
	layer_name = './data/abio/{0}'.format(layer_name)
	nodata = get_nodata(layer_name)
	if nodata == None:
		return False
	try:
		new_country_connected_components = Aoi.objects.filter(country=country)
		GridxxKmAoi = apps.get_model(app_label='updatedb.Grid{0}KmAoi'.format(xx))
		grid_cells = []
		number_connected_components = new_country_connected_components.count()
		for i in range(number_connected_components):
			grid_cells += list(GridxxKmAoi.objects.filter(the_geom__intersects=new_country_connected_components[i].geom))
	except Exception as e:
		logger.error("It can't get the entire grid for {0}".format(country))
		return False
	number_connected_components = None
	new_country_connected_components = None
	number_cells = len(grid_cells)
	try:
		bids = dict()
		for i in range(number_cells):
			gridid = getattr(grid_cells[i], 'gridid_{0}km'.format(xx))
			the_geom = json.loads(grid_cells[i].small_geom.geojson)
			with rasterio.open(layer_name) as src:
     			out_image, out_transform = mask(src, the_geom, nodata=nodata, crop=True)
			imageCountry = out_image[0]
			classes = np.unique(imageCountry)
			mapp = get_layer_mapping(layer)
			classes = [mapp[c] for c in set(classes) if c != 11]
			mapp = None
			# ToDo: Update layer field of cell
			grid_cells[i].save()
			for c in classes:
				if c in bids.keys():
					bids[c] = bids[c] + [gridid]
				else:
					bids[c] = [gridid]
			gridid = None
		for c in bids.keys():
			b = RasterBins.objects.get(bid=c)
			setattr(b, 'cells_{0}km'.format(xx), bids[c])
			b.save()
		bids = None
	except Exception as e:
		logger.error("It can't get the discretized raster for {0}".format(country))
		return False
	return True

def get_raster_name(layer):
	#files = os.listdir('../data/abio/')
	files = os.listdir('./data/abio/')
	name = ''
	for f in files:
		try:
			index = f.index(layer)
			index = f.index(".tif")
			name = f
			break
		except ValueError:
			continue
	return name

def get_layer_mapping(layer):
	bins = RasterBins.objects.filter(layer=layer)
	mapp = dict()
	for b in bins:
		mapp[b.icat] = b.bid
	return mapp

def get_nodata(layer):
	try:
		ds = gdal.Open(layer)
		band = ds.GetRasterBand(1)
		nodata = band.GetStatistics(True, True)[1]
		band = None
		ds = None
	except Exception as e:
		logger.error("no se pudo obtener nodata value de {0}".format(layer))
		return None
	return nodata