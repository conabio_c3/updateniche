import json
import logging
import datetime
from updatedb.serializers import SnibSerializer
from updatedb.models import Snib

logger = logging.getLogger(__name__)

def get_json(data, name_file):
	"""
	Funcion para generar archivo .json de un registro en snib
	"""
	try:
		snib = Snib.objects.get(**data)
		attributes = list(SnibSerializer._declared_fields)
		file = open("data/{0}.json".format(name_file), "w")
		dict_att = dict()
		for att in attributes:
			dict_att[att] = getattr(snib, att)
		dict_att["ultimafechaactualizacion"] = str(dict_att["ultimafechaactualizacion"])
		file.write(str(dict_att).replace("'", "\"").replace("None", "null"))
		file.close()
		logger.info("Archivo {0}.json creado!".format(name_file))
	except Exception as e:
		logger.error(str(e))

