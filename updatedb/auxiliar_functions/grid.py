from ..models import *
from django.db.models import F
from ..tasks import *
from celery import group

def update_grids(spid_dict_8km_animalia ,
        		spid_dict_16km_animalia ,
        		spid_dict_32km_animalia ,
        		spid_dict_64km_animalia ,
   				spid_dict_8km_plantae ,
        		spid_dict_16km_plantae ,
        		spid_dict_32km_plantae ,
				spid_dict_64km_plantae ,
        		spid_dict_8km_fungi ,
				spid_dict_16km_fungi ,
        		spid_dict_32km_fungi ,
				spid_dict_64km_fungi ,
        		spid_dict_8km_protoctista ,
				spid_dict_16km_protoctista ,
				spid_dict_32km_protoctista ,
        		spid_dict_64km_protoctista ,
				spid_dict_8km_prokaryotae ,
        		spid_dict_16km_prokaryotae ,
        		spid_dict_32km_prokaryotae ,
        		spid_dict_64km_prokaryotae ):
    for key in spid_dict_8km_animalia:
    	grid_8km = Grid8KmAoi.objects.filter(gridid_8km__in=spid_dict_8km_animalia[key])
    	grid_8km.update(animalia=F('animalia')+[key] if not key is F('animalia') else F('animalia'))
    for key in spid_dict_16km_animalia:
    	grid_16km = Grid16KmAoi.objects.filter(gridid_16km__in=spid_dict_16km_animalia[key])
    	grid_16km.update(animalia=F('animalia')+[key] if not key is F('animalia') else F('animalia'))
    for key in spid_dict_32km_animalia:
    	grid_32km = Grid32KmAoi.objects.filter(gridid_32km__in=spid_dict_32km_animalia[key])
    	grid_32km.update(animalia=F('animalia')+[key] if not key is F('animalia') else F('animalia'))
    for key in spid_dict_64km_animalia:
    	grid_64km = Grid64KmAoi.objects.filter(gridid_64km__in=spid_dict_64km_animalia[key])
    	grid_64km.update(animalia=F('animalia')+[key] if not key is F('animalia') else F('animalia'))

    for key in spid_dict_8km_plantae:
    	grid_8km = Grid8KmAoi.objects.filter(gridid_8km__in=spid_dict_8km_plantae[key])
    	grid_8km.update(plantae=F('plantae')+[key] if not key is F('plantae') else F('plantae'))
    for key in spid_dict_16km_plantae:
    	grid_16km = Grid16KmAoi.objects.filter(gridid_16km__in=spid_dict_16km_plantae[key])
    	grid_16km.update(plantae=F('plantae')+[key] if not key is F('plantae') else F('plantae'))
    for key in spid_dict_32km_plantae:
    	grid_32km = Grid32KmAoi.objects.filter(gridid_32km__in=spid_dict_32km_plantae[key])
    	grid_32km.update(plantae=F('plantae')+[key] if not key is F('plantae') else F('plantae'))
    for key in spid_dict_64km_plantae:
    	grid_64km = Grid64KmAoi.objects.filter(gridid_64km__in=spid_dict_64km_plantae[key])
    	grid_64km.update(plantae=F('plantae')+[key] if not key is F('plantae') else F('plantae'))

    for key in spid_dict_8km_fungi:
    	grid_8km = Grid8KmAoi.objects.filter(gridid_8km__in=spid_dict_8km_fungi[key])
    	grid_8km.update(fungi=F('fungi')+[key] if not key in F('fungi') else F('fungi'))
    for key in spid_dict_16km_fungi:
    	grid_16km = Grid16KmAoi.objects.filter(gridid_16km__in=spid_dict_16km_fungi[key])
    	grid_16km.update(fungi=F('fungi')+[key] if not key in F('fungi') else F('fungi'))
    for key in spid_dict_32km_fungi:
    	grid_32km = Grid32KmAoi.objects.filter(gridid_32km__in=spid_dict_32km_fungi[key])
    	grid_32km.update(fungi=F('fungi')+[key] if not key in F('fungi') else F('fungi'))
    for key in spid_dict_64km_fungi:
    	grid_64km = Grid64KmAoi.objects.filter(gridid_64km__in=spid_dict_64km_fungi[key])
    	grid_64km.update(fungi=F('fungi')+[key] if not key in F('fungi') else F('fungi'))

    for key in spid_dict_8km_protoctista:
    	grid_8km = Grid8KmAoi.objects.filter(gridid_8km__in=spid_dict_8km_protoctista[key])
    	grid_8km.update(protoctista=F('protoctista')+[key] if not key in F('protoctista') else F('protoctista'))
    for key in spid_dict_16km_protoctista:
    	grid_16km = Grid16KmAoi.objects.filter(gridid_16km__in=spid_dict_16km_protoctista[key])
    	grid_16km.update(protoctista=F('protoctista')+[key] if not key in F('protoctista') else F('protoctista'))
    for key in spid_dict_32km_protoctista:
    	grid_32km = Grid32KmAoi.objects.filter(gridid_32km__in=spid_dict_32km_protoctista[key])
    	grid_32km.update(protoctista=F('protoctista')+[key] if not key in F('protoctista') else F('protoctista'))
    for key in spid_dict_64km_protoctista:
    	grid_64km = Grid64KmAoi.objects.filter(gridid_64km__in=spid_dict_64km_protoctista[key])
    	grid_64km.update(protoctista=F('protoctista')+[key] if not key in F('protoctista') else F('protoctista'))

    for key in spid_dict_8km_prokaryotae:
    	grid_8km = Grid8KmAoi.objects.filter(gridid_8km__in=spid_dict_8km_prokaryotae[key])
    	grid_8km.update(prokaryotae=F('prokaryotae')+[key] if not key in F('prokaryotae') else F('prokaryotae'))
    for key in spid_dict_16km_prokaryotae:
    	grid_16km = Grid16KmAoi.objects.filter(gridid_16km__in=spid_dict_16km_prokaryotae[key])
    	grid_16km.update(prokaryotae=F('prokaryotae')+[key] if not key in F('prokaryotae') else F('prokaryotae'))
    for key in spid_dict_32km_prokaryotae:
    	grid_32km = Grid32KmAoi.objects.filter(gridid_32km__in=spid_dict_32km_prokaryotae[key])
    	grid_32km.update(prokaryotae=F('prokaryotae')+[key] if not key in F('prokaryotae') else F('prokaryotae'))
    for key in spid_dict_64km_prokaryotae:
    	grid_64km = Grid64KmAoi.objects.filter(gridid_64km__in=spid_dict_64km_prokaryotae[key])
    	grid_64km.update(prokaryotae=F('prokaryotae')+[key] if not key in F('prokaryotae') else F('prokaryotae'))

def correct_grid_errors(kingdom):
    spid_dict_8km_animalia =  dict()
    spid_dict_16km_animalia =  dict()
    spid_dict_32km_animalia =  dict()
    spid_dict_64km_animalia =  dict()
    spid_dict_8km_plantae =  dict()
    spid_dict_16km_plantae =  dict()
    spid_dict_32km_plantae =  dict()
    spid_dict_64km_plantae =  dict()
    spid_dict_8km_fungi =  dict()
    spid_dict_16km_fungi =  dict()
    spid_dict_32km_fungi =  dict()
    spid_dict_64km_fungi =  dict()
    spid_dict_8km_protoctista =  dict()
    spid_dict_16km_protoctista =  dict()
    spid_dict_32km_protoctista =  dict()
    spid_dict_64km_protoctista =  dict()
    spid_dict_8km_prokaryotae =  dict()
    spid_dict_16km_prokaryotae =  dict()
    spid_dict_32km_prokaryotae =  dict()
    spid_dict_64km_prokaryotae =  dict()
    snibs = Snib.objects.filter(reinovalido=kingdom)
    print("{} ocurrences finded".format(snibs.count()))
    for snib in snibs:
        try:
            intersection = Grid8KmAoi.objects.get(the_geom__intersects=snib.the_geom)
        except Exception as e:
            logger.warn("{0} for snib {1}".format(str(e), snib.idejemplar))
        spid = snib.spid.spid
        if snib.reinovalido == 'Animalia':
            if spid in spid_dict_8km_animalia: 
                spid_dict_8km_animalia[spid].append(intersection.gridid_8km)
            else:
                spid_dict_8km_animalia[spid] = [intersection.gridid_8km]
            if spid in spid_dict_16km_animalia: 
                spid_dict_16km_animalia[spid].append(intersection.gridid_16km)
            else:
                spid_dict_16km_animalia[spid] = [intersection.gridid_16km]
            if spid in spid_dict_32km_animalia: 
                spid_dict_32km_animalia[spid].append(intersection.gridid_32km)
            else:
                spid_dict_32km_animalia[spid] = [intersection.gridid_32km]
            if spid in spid_dict_64km_animalia: 
                spid_dict_64km_animalia[spid].append(intersection.gridid_64km)
            else:
                spid_dict_64km_animalia[spid] = [intersection.gridid_64km]
        elif snib.reinovalido == 'Plantae':
            if spid in spid_dict_8km_plantae: 
                spid_dict_8km_plantae[spid].append(intersection.gridid_8km)
            else:
                spid_dict_8km_plantae[spid] = [intersection.gridid_8km]
            if spid in spid_dict_16km_plantae: 
                spid_dict_16km_plantae[spid].append(intersection.gridid_16km)
            else:
                spid_dict_16km_plantae[spid] = [intersection.gridid_16km]
            if spid in spid_dict_32km_plantae: 
                spid_dict_32km_plantae[spid].append(intersection.gridid_32km)
            else:
                spid_dict_32km_plantae[spid] = [intersection.gridid_32km]
            if spid in spid_dict_64km_plantae: 
                spid_dict_64km_plantae[spid].append(intersection.gridid_64km)
            else:
                spid_dict_64km_plantae[spid] = [intersection.gridid_64km]
        elif snib.reinovalido == 'Fungi':
            if spid in spid_dict_8km_fungi: 
                spid_dict_8km_fungi[spid].append(intersection.gridid_8km)
            else:
                spid_dict_8km_fungi[spid] = [intersection.gridid_8km]
            if spid in spid_dict_16km_fungi: 
                spid_dict_16km_fungi[spid].append(intersection.gridid_16km)
            else:
                spid_dict_16km_fungi[spid] = [intersection.gridid_16km]
            if spid in spid_dict_32km_fungi: 
                spid_dict_32km_fungi[spid].append(intersection.gridid_32km)
            else:
                spid_dict_32km_fungi[spid] = [intersection.gridid_32km]
            if spid in spid_dict_64km_fungi: 
                spid_dict_64km_fungi[spid].append(intersection.gridid_64km)
            else:
                spid_dict_64km_fungi[spid] = [intersection.gridid_64km]
        elif snib.reinovalido == 'Protoctista':
            if spid in spid_dict_8km_protoctista: 
                spid_dict_8km_protoctista[spid].append(intersection.gridid_8km)
            else:
                spid_dict_8km_protoctista[spid] = [intersection.gridid_8km]
            if spid in spid_dict_16km_protoctista: 
                spid_dict_16km_protoctista[spid].append(intersection.gridid_16km)
            else:
                spid_dict_16km_protoctista[spid] = [intersection.gridid_16km]
            if spid in spid_dict_32km_protoctista: 
                spid_dict_32km_protoctista[spid].append(intersection.gridid_32km)
            else:
                spid_dict_32km_protoctista[spid] = [intersection.gridid_32km]
            if spid in spid_dict_64km_protoctista: 
                spid_dict_64km_protoctista[spid].append(intersection.gridid_64km)
            else:
                spid_dict_64km_protoctista[spid] = [intersection.gridid_64km]
        elif snib.reinovalido == 'Prokaryotae':
            if spid in spid_dict_8km_prokaryotae: 
                spid_dict_8km_prokaryotae[spid].append(intersection.gridid_8km)
            else:
                spid_dict_8km_prokaryotae[spid] = [intersection.gridid_8km]
            if spid in spid_dict_16km_prokaryotae: 
                spid_dict_16km_prokaryotae[spid].append(intersection.gridid_16km)
            else:
                spid_dict_16km_prokaryotae[spid] = [intersection.gridid_16km]
            if spid in spid_dict_32km_prokaryotae: 
                spid_dict_32km_prokaryotae[spid].append(intersection.gridid_32km)
            else:
                spid_dict_32km_prokaryotae[spid] = [intersection.gridid_32km]
            if spid in spid_dict_64km_prokaryotae: 
                spid_dict_64km_prokaryotae[spid].append(intersection.gridid_64km)
            else:
                spid_dict_64km_prokaryotae[spid] = [intersection.gridid_64km]

    jobs = group(update_grids_animalia.s(spid_dict_8km_animalia ,
                    spid_dict_16km_animalia ,
                    spid_dict_32km_animalia ,
                    spid_dict_64km_animalia),
                update_grids_plantae.s(spid_dict_8km_plantae ,
                    spid_dict_16km_plantae ,
                    spid_dict_32km_plantae ,
                    spid_dict_64km_plantae),
                update_grids_fungi.s(spid_dict_8km_fungi ,
                    spid_dict_16km_fungi ,
                    spid_dict_32km_fungi ,
                    spid_dict_64km_fungi),
                update_grids_protoctista.s(spid_dict_8km_protoctista ,
                    spid_dict_16km_protoctista ,
                    spid_dict_32km_protoctista ,
                    spid_dict_64km_protoctista ),
                update_grids_prokaryotae.s(spid_dict_8km_prokaryotae ,
                    spid_dict_16km_prokaryotae ,
                    spid_dict_32km_prokaryotae ,
                    spid_dict_64km_prokaryotae))
    result = jobs.apply_async()