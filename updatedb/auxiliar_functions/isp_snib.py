from django.db.models import Q
from ..models.species import *
from ..models.occurrence import *

def isp_data(snib):
	return {'reinovalido':snib.reinovalido,'phylumdivisionvalido':snib.phylumdivisionvalido,'clasevalida':snib.clasevalida,'ordenvalido':snib.ordenvalido,'familiavalida':snib.familiavalida,'generovalido':snib.generovalido,'especievalidabusqueda':snib.especievalidabusqueda,'especievalida':snib.especievalida,'categoriainfraespecievalida':snib.categoriainfraespecievalida}

def correct_multiples_isp_returned(data):
	isps = IspSnib.objects.filter(Q(reinovalido=data['reinovalido']) &
                                  Q(phylumdivisionvalido=data['phylumdivisionvalido']) &
                                  Q(clasevalida=data['clasevalida']) &
                                  Q(ordenvalido=data['ordenvalido']) &
                                  Q(familiavalida=data['familiavalida']) &
                                  Q(generovalido=data['generovalido']) &
                                  Q(especievalidabusqueda=data['especievalidabusqueda']) &
                                  Q(especievalida=data['especievalida']) &
                                  Q(categoriainfraespecievalida=data['categoriainfraespecievalida'])).order_by('ispid')
	re = isps.count()
	if re > 1 :
		isp = isps[0]
		repeated_isps = IspSnib.objects.filter(Q(reinovalido=isp.reinovalido) &
			                                  Q(phylumdivisionvalido=isp.phylumdivisionvalido) &
			                                  Q(clasevalida=isp.clasevalida) &
			                                  Q(ordenvalido=isp.ordenvalido) &
			                                  Q(familiavalida=isp.familiavalida) &
			                                  Q(generovalido=isp.generovalido) &
			                                  Q(especievalidabusqueda=isp.especievalidabusqueda) &
			                                  Q(especievalida=isp.especievalida) &
			                                  Q(categoriainfraespecievalida=isp.categoriainfraespecievalida) & 
			                                  ~ Q(ispid=isp.ispid))
		re2 = repeated_isps.count()
		if re == re2+1:
			for irep in repeated_isps:
				Snib.objects.filter(ispid=irep).update(ispid=isp)
				print(str(irep.ispid) + " isp deleted")
				irep.delete()