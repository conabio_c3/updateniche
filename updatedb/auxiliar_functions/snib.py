from ..models import *
import logging
from django.contrib.gis.geos import GEOSGeometry, Point
from django.db.models import Q
from ..tasks import *

logger = logging.getLogger(__name__)

def compare_snib_fields(snib, data):
    """Compara los campos de un registro de la tabla snib
    """
    try:
        snib_aux = Snib.objects.get(**data)
    except Exception as e:
        snib_aux = None
    if snib_aux == None:
        return False
    return True

def update_snib_fields(snib, data):
    """Actualiza los campos de un registro de la tabla snib
    """
    # correct geo_snib
    new_point = GEOSGeometry('Point({0} {1})'.format(data['longitud'], data['latitud']), srid=4326) 
    try:
        intersection = Grid8KmAoi.objects.get(the_geom__intersects=new_point)
    except Exception as e:
        logger.warn("{0} for snib {1}".format(str(e), snib.idejemplar))
        return None
    if data['longitud'] != snib.longitud or data['latitud'] != snib.latitud:
        logger.info('Cambiando coordenadas de {0}, ({1}, {2}) -> ({3}, {4}) '.format(snib.idejemplar, snib.longitud, snib.latitud, data['longitud'], data['latitud']))
        try:
            geosnib = GeoSnib.objects.get(the_geom=new_point)
        except GeoSnib.DoesNotExist:
            geosnib = GeoSnib(longitud=data['longitud'], latitud=data['latitud'])
            point900913 = new_point
            point900913.transform(900913)
            geosnib.geom_m = point900913
            geosnib.the_geom = new_point
            geosnib.gridid_64km = intersection.gridid_64km
            geosnib.gridid_32km = intersection.gridid_32km
            geosnib.gridid_16km = intersection.gridid_16km
            geosnib.gridid_8km = intersection.gridid_8km
            geosnib.save()
        except GeoSnib.MultipleObjectsReturned:
            correct_multiples_geo_returned({'longitud':data['longitud'],'latitud':data['latitud']})
            geosnib = GeoSnib.objects.get(the_geom=new_point)
        except Exception as e:
            logger.warn('La actualizacion de snib con ID {0} no se compelto por {1}'.format(snib.idejemplar, str(e)))            
            return None
        snib.geoid = geosnib
        snib.the_geom = new_point
        snib.longitud = data['longitud']
        snib.latitud = data['latitud']
        snib.gridid_8km = intersection.gridid_8km
        snib.gridid_16km = intersection.gridid_16km
        snib.gridid_32km = intersection.gridid_32km
        snib.gridid_64km = intersection.gridid_64km

    # correct sp_snib
    if data['especievalidabusqueda'] != snib.especievalidabusqueda or data['phylumdivisionvalido'] != snib.phylumdivisionvalido or data['reinovalido'] != snib.reinovalido:
        logger.info('Cambiando sp de {0}, ({1}, {2}, {3}) -> ({4}, {5}, {6}) '.format(snib.idejemplar, snib.especievalidabusqueda, snib.phylumdivisionvalido, snib.reinovalido, data['especievalidabusqueda'], data['phylumdivisionvalido'], data['reinovalido']))
        try:
            spsnib = SpSnib.objects.get(Q(especievalidabusqueda=data["especievalidabusqueda"]) &
                                        Q(phylumdivisionvalido=data["phylumdivisionvalido"]) &
                                        Q(reinovalido=data["reinovalido"]))
        except SpSnib.DoesNotExist:
            spsnib = SpSnib(reinovalido=data['reinovalido'], phylumdivisionvalido=data['phylumdivisionvalido'], 
                            clasevalida=data['clasevalida'], ordenvalido=data['ordenvalido'], 
                            familiavalida=data['familiavalida'], generovalido=data['generovalido'], 
                            especievalidabusqueda=data['especievalidabusqueda'])
            spsnib.cells_64km = [intersection.gridid_64km]
            spsnib.cells_32km = [intersection.gridid_32km]
            spsnib.cells_16km = [intersection.gridid_16km]
            spsnib.cells_8km  = [intersection.gridid_8km]
            spsnib.save()
        except SpSnib.MultipleObjectsReturned:
            correct_multiples_sp_returned({'reinovalido':data['reinovalido'], 'phylumdivisionvalido':data['phylumdivisionvalido'], 
                                            'clasevalida':data['clasevalida'], 'ordenvalido':data['ordenvalido'], 
                                            'familiavalida':data['familiavalida'], 'generovalido':data['generovalido'], 
                                            'especievalidabusqueda':data['especievalidabusqueda']})
            spsnib = SpSnib.objects.get(Q(especievalidabusqueda=data["especievalidabusqueda"]) &
                                        Q(phylumdivisionvalido=data["phylumdivisionvalido"]) &
                                        Q(reinovalido=data["reinovalido"]))
        except Exception as e:
            logger.warn('La actualizacion de snib con ID {0} no se compelto por {1}'.format(snib.idejemplar, str(e)))            
            return None
        snib.spid = spsnib
        snib.especievalidabusqueda = data['especievalidabusqueda']
        snib.phylumdivisionvalido = data['phylumdivisionvalido']
        snib.reinovalido = data['reinovalido']
        snib.clasevalida = data['clasevalida']
        snib.ordenvalido = data['ordenvalido']
        snib.familiavalida = data['familiavalida']

    #correct isp_snib
    if data['especievalidabusqueda'] != snib.especievalidabusqueda or data['phylumdivisionvalido'] != snib.phylumdivisionvalido or data['reinovalido'] != snib.reinovalido or snib.categoriainfraespecievalida != data['categoriainfraespecievalida']:
        logger.info('Cambiando isp de {0}, ({1}, {2}, {3}, {4}) -> ({5}, {6}, {7}, {8}) '.format(snib.idejemplar, snib.especievalidabusqueda, snib.phylumdivisionvalido, snib.reinovalido, snib.categoriainfraespecievalida, data['especievalidabusqueda'], data['phylumdivisionvalido'], data['reinovalido'], data['categoriainfraespecievalida']))
        try:
            ispsnib = IspSnib.objects.get(Q(reinovalido=data['reinovalido']) &
                                          Q(phylumdivisionvalido=data['phylumdivisionvalido']) &
                                          Q(especievalidabusqueda=data['especievalidabusqueda']) &
                                          Q(categoriainfraespecievalida=data['categoriainfraespecievalida']))
        except IspSnib.DoesNotExist:
            ispsnib = IspSnib({'reinovalido':data['reinovalido'],'phylumdivisionvalido':data['phylumdivisionvalido'],
                                'clasevalida':data['clasevalida'],'ordenvalido':data['ordenvalido'],
                                'familiavalida':data['familiavalida'], 'generovalido':data['generovalido'],
                                'especievalidabusqueda':data['especievalidabusqueda'], 'especievalida':data['especievalida'],
                                'categoriainfraespecievalida':data['categoriainfraespecievalida']})
            ispsnib.spid = spsnib
            ispsnib.cells_64km = [intersection.gridid_64km]
            ispsnib.cells_32km = [intersection.gridid_32km]
            ispsnib.cells_16km = [intersection.gridid_16km]
            ispsnib.cells_8km  = [intersection.gridid_8km]
            ispsnib.save()
        except IspSnib.MultipleObjectsReturned:
            correct_multiples_isp_returned({'reinovalido':data['reinovalido'],'phylumdivisionvalido':data['phylumdivisionvalido'],
                                            'clasevalida':data['clasevalida'],'ordenvalido':data['ordenvalido'],
                                            'familiavalida':data['familiavalida'], 'generovalido':data['generovalido'],
                                            'especievalidabusqueda':data['especievalidabusqueda'], 'especievalida':data['especievalida'],
                                            'categoriainfraespecievalida':data['categoriainfraespecievalida']})
            ispsnib = IspSnib.objects.get(Q(reinovalido=data['reinovalido']) &
                                          Q(phylumdivisionvalido=data['phylumdivisionvalido']) &
                                          Q(especievalidabusqueda=data['especievalidabusqueda']) &
                                          Q(categoriainfraespecievalida=data['categoriainfraespecievalida']))
        except Exception as e:
            logger.warn('La actualizacion de snib con ID {0} no se compelto por {1}'.format(snib.idejemplar, str(e)))            
            return None
        snib.ispid = ispsnib
        snib.especievalidabusqueda = data['especievalidabusqueda']
        snib.phylumdivisionvalido = data['phylumdivisionvalido']
        snib.reinovalido = data['reinovalido']
        snib.clasevalida = data['clasevalida']
        snib.ordenvalido = data['ordenvalido']
        snib.familiavalida = data['familiavalida']
        snib.categoriainfraespecievalida = data['categoriainfraespecievalida']
    
    snib.save()
    for field in data.keys():
        if getattr(snib, field) != data[field]:
            setattr(snib, field, data[field])
    return snib

def update_all_ispids():
    isps = IspSnib.objects.all().order_by('ispid')
    isps = [isp.ispid for isp in isps]
    nisps = len(isps)
    logger.info("There are {0} infraspecies".format(nisps))
    for i in range(nisps):
        ispid = isps[i]
        logger.info("updating ocurrences with ispid {0}".format(ispid))
        update_specific_ispid.delay(ispid)

def update_all_geoids():
    geos = GeoSnib.objects.all().order_by('geoid')
    geos = [geo.geoid for geo in geos]
    ngeos = len(geos)
    logger.info("There are {0} points".format(ngeos))
    for i in range(ngeos):
        geoid = geos[i]
        logger.info("updating ocurrences with geoid {0}".format(geoid))
        update_specific_geos.delay(geoid)

def update_all_ispids_from(initial_ispid):
    isps = IspSnib.objects.filter(ispid__gt=initial_ispid).order_by('ispid')
    isps = [isp.ispid for isp in isps]
    nisps = len(isps)
    logger.info("There are {0} infraspecies".format(nisps))
    for i in range(nisps):
        ispid = isps[i]
        logger.info("updating ocurrences with ispid {0}".format(ispid))
        update_specific_ispid.delay(ispid)

def update_all_geoids_from(initial_geoid):
    geos = GeoSnib.objects.filter(geoid__gt=initial_geoid).order_by('geoid')
    geos = [geo.geoid for geo in geos]
    ngeos = len(geos)
    logger.info("There are {0} points".format(ngeos))
    for i in range(ngeos):
        geoid = geos[i]
        logger.info("updating ocurrences with geoid {0}".format(geoid))
        update_specific_geos.delay(geoid)

def update_all_ispids_from_to(initial_ispid, final_ispid):
    isps = IspSnib.objects.filter(ispid__gt=initial_ispid, ispid__lt=final_ispid).order_by('ispid')
    isps = [isp.ispid for isp in isps]
    nisps = len(isps)
    logger.info("There are {0} infraspecies".format(nisps))
    for i in range(nisps):
        ispid = isps[i]
        logger.info("updating ocurrences with ispid {0}".format(ispid))
        update_specific_ispid.delay(ispid)

def update_all_geoids_from_to(initial_geoid, final_geoid):
    geos = GeoSnib.objects.filter(geoid__gt=initial_geoid, geoid__lt=final_geoid).order_by('geoid')
    geos = [geo.geoid for geo in geos]
    ngeos = len(geos)
    logger.info("There are {0} points".format(ngeos))
    for i in range(ngeos):
        geoid = geos[i]
        logger.info("updating ocurrences with geoid {0}".format(geoid))
        update_specific_geos.delay(geoid)

def update_all_gridid_geoids_from_to(initial_geoid, final_geoid):
    geos = GeoSnib.objects.filter(geoid__gt=initial_geoid, geoid__lt=final_geoid).order_by('geoid')
    geos = [geo.geoid for geo in geos]
    ngeos = len(geos)
    logger.info("There are {0} points".format(ngeos))
    for i in range(ngeos):
        geoid = geos[i]
        logger.info("updating ocurrences with geoid {0}".format(geoid))
        update_gridid_geos.delay(geoid)

def update_all_geom_geoids_from_to(initial_geoid, final_geoid):
    geos = GeoSnib.objects.filter(geoid__gt=initial_geoid, geoid__lt=final_geoid).order_by('geoid')
    geos = [geo.geoid for geo in geos]
    ngeos = len(geos)
    logger.info("There are {0} points".format(ngeos))
    for i in range(ngeos):
        geoid = geos[i]
        logger.info("updating ocurrences with geoid {0}".format(geoid))
        update_geom_geos.delay(geoid)