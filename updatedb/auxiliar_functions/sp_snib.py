from ..models import *
import logging
import time

logger = logging.getLogger(__name__)

def get_sp_craniata():
    sp_craniata = SpSnib.objects.filter(phylumdivisionvalido="Craniata")
    sp_chordata_names = []
    for spc in sp_craniata:
        sp_chordata_names.append(spc.especievalidabusqueda)
    return SpSnib.objects.filter(especievalidabusqueda__in=sp_chordata_names).filter(phylumdivisionvalido="Chordata")[0:4]  

def correct_craniata():
    sp_chordata = get_sp_craniata()
    for sp in sp_chordata:
        spid_2 = sp.spid
        spid_1 = change_to_chordata(sp)
        snibs = Snib.objects.filter(spid=spid_2)
        for snib in snibs:
            change_spid_grid8kmaoi(spid_1, spid_2, snib.gridid_8km)
            change_spid_grid16kmaoi(spid_1, spid_2, snib.gridid_16km)
            change_spid_grid32kmaoi(spid_1, spid_2, snib.gridid_32km)
            change_spid_grid64kmaoi(spid_1, spid_2, snib.gridid_64km)
        Snib.objects.filter(spid=spid_2).update(spid=spid_1)

def change_to_chordata(sp):
    sp_c = SpSnib.objects.get(especievalidabusqueda=sp.especievalidabusqueda, phylumdivisionvalido="Craniata")
    sp_c.phylumdivisionvalido = "Chordata"
    sp_c.cells_64km = sp_c.cells_64km + sp.cells_64km
    sp_c.cells_32km = sp_c.cells_32km + sp.cells_32km
    sp_c.cells_16km = sp_c.cells_16km + sp.cells_16km
    sp_c.cells_8km = sp_c.cells_8km + sp.cells_8km
    sp_c.save()
    sp.delete()

    isp_c_list = IspSnib.objects.filter(spid=sp_c.spid, especievalidabusqueda=sp.especievalidabusqueda, phylumdivisionvalido="Craniata")
    
    for isp_c in isp_c_list:    
        isp_list = IspSnib.objects.filter(spid=sp_c.spid, especievalidabusqueda=sp.especievalidabusqueda, phylumdivisionvalido="Chordata", categoriainfraespecievalida=isp_c.categoriainfraespecievalida)
        for isp in isp_list: 
            isp_c.phylumdivisionvalido = "Chordata"
            isp_c.cells_64km = isp_c.cells_64km + isp.cells_64km
            isp_c.cells_32km = isp_c.cells_32km + isp.cells_32km
            isp_c.cells_16km = isp_c.cells_16km + isp.cells_16km
            isp_c.cells_8km = isp_c.cells_8km + isp.cells_8km
            isp_c.save()
            isp.delete()
    logger.info("specie with spid={0} changed to Chordata".format(sp_c.spid))
    return sp_c.spid

def change_spid_grid8kmaoi(spid_1, spid_2, gridid):
    grid =  Grid8KmAoi.objects.get(gridid_8km=gridid)
    try:
        grid.animalia.remove(spid_2)
    except Exception as e:
        print(str(e))
    if not spid_1 in grid.animalia:
        grid.animalia.append(spid_1)
    grid.save()

def change_spid_grid16kmaoi(spid_1, spid_2, gridid):
    grid =  Grid16KmAoi.objects.get(gridid_16km=gridid)
    try:
        grid.animalia.remove(spid_2)
    except Exception as e:
        print(str(e))
    if not spid_1 in grid.animalia:
        grid.animalia.append(spid_1)
    grid.save()

def change_spid_grid32kmaoi(spid_1, spid_2, gridid):
    grid =  Grid32KmAoi.objects.get(gridid_32km=gridid)
    try:
        grid.animalia.remove(spid_2)
    except Exception as e:
        print(str(e))
    if not spid_1 in grid.animalia:
        grid.animalia.append(spid_1)
    grid.save()

def change_spid_grid64kmaoi(spid_1, spid_2, gridid):
    grid =  Grid64KmAoi.objects.get(gridid_64km=gridid)
    try:
        grid.animalia.remove(spid_2)
    except Exception as e:
        print(str(e))
    if not spid_1 in grid.animalia:
        grid.animalia.append(spid_1)
    grid.save()

def sp_data(snib):
    return { 'reinovalido':snib.reinovalido, 'phylumdivisionvalido':snib.phylumdivisionvalido, 'clasevalida':snib.clasevalida, 'ordenvalido':snib.ordenvalido, 'familiavalida':snib.familiavalida, 'generovalido':snib.generovalido, 'especievalidabusqueda':snib.especievalidabusqueda }

def correct_multiples_sp_returned(data):
    sps = SpSnib.objects.filter(Q(especievalidabusqueda=data["especievalidabusqueda"]) &
                                Q(phylumdivisionvalido=data["phylumdivisionvalido"]) &
                                Q(reinovalido=data["reinovalido"])).order_by('spid')
    re = sps.count()
    if re > 1:
        sp = sps[0]
        repeated_sps = SpSnib.objects.filter(Q(especievalidabusqueda=data["especievalidabusqueda"]) &
                                             Q(phylumdivisionvalido=data["phylumdivisionvalido"]) &
                                             Q(reinovalido=data["reinovalido"]) &
                                             ~ Q(spid=sp.spid))
        re2 = repeated_sps.count()
        if re == re2 + 1:
            for srep in repeated_sps:
                Snib.objects.filter(spid=srep).update(spid=sp)
                print(str(srep.spid) + "sp deleted!")
                srep.delete() 