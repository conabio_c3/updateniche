from django.db.models import Q
from ..models.geosnib import *
from ..models.occurrence import *
from django.contrib.gis.geos import GEOSGeometry, Point

def geo_data(snib):
	return {'longitud':snib.longitud,'latitud':snib.latitud,}
	 

def geo_default(snib, intersection):
	return {'the_geom': snib.the_geom,'gridid_64km' : intersection.gridid_64km,'gridid_32km' : intersection.gridid_32km,'gridid_16km' : intersection.gridid_16km,'gridid_8km'  : intersection.gridid_8km,}

def correct_multiples_geo_returned(data):
	geom = GEOSGeometry('Point({0} {1})'.format(data['longitud'], data['latitud']), srid=4326)
	points = GeoSnib.objects.filter(the_geom=geom).order_by('geoid')
	re = points.count()
	if re > 1 :
		point = points[0]
		repeated_points = GeoSnib.objects.filter(Q(the_geom=geom) & 
												 ~ Q(geoid=point.geoid))
		re2 = repeated_points.count()
		if re == re2+1:
			for prep in repeated_points:
				print(str(prep.geoid) + " point deleted")
				Snib.objects.filter(geoid=prep).update(geoid=point)
				prep.delete()
