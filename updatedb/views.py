import csv
import json
import logging
import time
import requests
import sys

from django.core.files import File
from django.http import HttpRequest
from django.shortcuts import render
from django.urls import reverse
from rest_framework import (renderers,
                            status)
from rest_framework.decorators import (api_view,
                                       renderer_classes)
from rest_framework.response import Response
from rest_framework.schemas import AutoSchema
from rest_framework.views import APIView
from .models import (GeoSnib,
                     Snib,
                     SpSnib,
                     Grid8KmAoi)
from .serializers import *
from .auxiliar_functions.snib import *


logger = logging.getLogger(__name__)

class GeoSnibView(APIView):
    """
    Servicio para insertar u obtener el geoid de un registro de un punto geografico en la tabla *geo_snib* de manera independiente a partir de un json con los siguientes campos.
    """
    def post(self, request):
        geosnib = None
        serial = GeoSnibSerializer(data=request.data)
        if serial.is_valid():
            try:
                geosnib = GeoSnib.objects.get(**request.data)
            except Exception:
                geosnib = None
            if geosnib != None:
                return Response({
                                    'status':'success',
                                    'message':'geo_snib row already exist!',
                                    'geoid':geosnib.geoid
                                }, status.HTTP_200_OK)
            try:
                geosnib = serial.save()
            except Exception as e:
                logger.error(e)
                return Response({
                                    'status':'error',
                                    'message': str(e),
                                    'geoid': None,
                                }, status.HTTP_500_INTERNAL_SERVER_ERROR)
            logger.info("GeoSnib saved !!")
            return Response({
                                'status': 'success',
                                'message':'geo_snib row inserted!',
                                'geoid':geosnib.geoid
                            }, status.HTTP_201_CREATED)
        return Response({
                            'status':'error',
                            'message':'no valid data',
                            'geoid': None,
                        }, status.HTTP_400_BAD_REQUEST)

    def get_serializer(self):
        return GeoSnibSerializer()

class SpSnibView(APIView):
    """
    Servicio para insertar un registro en la tabla *snib*, haciendo los registros correspondientes de especies en la tabla *sp_snib*, de infraespecies en *isp_snib* y de puntos geograficos en *geo_snib*, a partir de un json con los siguientes campos.
    """
    def post(self, request):
        spsnib = None
        serial = SpSnibSerializer(data=request.data)
        if serial.is_valid():
            try:
                spsnib = SpSnib.objects.get(**request.data)
            except SpSnib.DoesNotExist:
                spsnib = None
            if spsnib != None:
                return Response({
                                    'status':'success',
                                    'message':'spsnib row already exist!',
                                    'spid': spsnib.spid,
                                }, status.HTTP_200_OK)
            try:
                spsnib = serial.save()
            except Exception as e:
                logger.error(e)
                return Response({
                                    'status':'error',
                                    'message': str(e),
                                    'spid': None,
                                }, status.HTTP_500_INTERNAL_SERVER_ERROR)
            logger.info("SpSinb saved !!")
            return Response({
                                'status': 'success',
                                'message':'spsnib row inserted!',
                                'spid': spsnib.spid
                            }, status.HTTP_201_CREATED)
        return Response({
                            'status':'error',
                            'message':'no valid data',
                            'spid': None,
                        }, status.HTTP_400_BAD_REQUEST)

    def get_serializer(self):
        return SpSnibSerializer()

class IspSnibView(APIView):
    """
    Servicio para insertar u obtener el ispid de un registro de una especie en la tabla *isp_snib* de manera independiente apartir de un json con los siguientes campos.
    """
    def post(self, reqiquest):
        ispsnib = None
        serial = IspSnibSerializer(data=request.data)
        if serial.is_valid():
            try:
                ispsnib = IspSnib.objects.get(**request.data)
            except IspSnib.DoesNotExist:
                ispsnib = None
            if ispsnib != None:
                return Response({
                                    'status':'success',
                                    'message':'ispsnib row already exist!',
                                    'ispid': ispsnib.ispid,
                                }, status.HTTP_200_OK)
            try:
                ispsnib = serial.save()
            except Exception as e:
                logger.error(e)
                return Response({
                                    'status':'error',
                                    'message': str(e),
                                    'ispid':None,
                                }, status.HTTP_500_INTERNAL_SERVER_ERROR)
            logger.info("IspSnib saved !!")
            return Response({
                                'status': 'success',
                                'message':'ispsnib row inserted!',
                                'ispid':ispsnib.ispid,
                            }, status.HTTP_201_CREATED)
        return Response({
                            'status': 'error',
                            'message':'no valid data',
                            'ispid':None
                        }, status.HTTP_400_BAD_REQUEST)

    def get_serializer(self):
        return IspSnibSerializer()

class SnibView(APIView):
    """ Servicio para insertar u obtener el spid de un registro de una especie
    en la tabla *sp_snib* de manera independiente apartir de un json con los
    siguientes campos.
    """
    def post(self, request):
        snib = None
        try:
            request.data["altitudmapa"] = int(request.data["altitudmapa"])
        except:
            pass
        serial = SnibSerializer(data=request.data)
        if serial.is_valid():
            try:
                snib = Snib.objects.get(idejemplar=request.data["idejemplar"])
            except Snib.DoesNotExist:
                snib = None
            if snib != None:
                update_message = ""
                if not compare_snib_fields(snib, request.data):
                    try:
                        snib = update_snib_fields(snib, request.data)
                        snib.save()
                        update_message = " and it was updated with new data"
                        logger.info("Campos de {0} actualizados".format(
                                    snib.idejemplar))
                    except Exception as e:
                        logger.info(str(e))
                return Response({'status': 'success',
                                 'message': 'snib row already exist{0}!'.format(
                                     update_message),
                                 'idejemplar': snib.idejemplar,
                                }, status.HTTP_200_OK)
            try:
                snib = serial.save()
                logger.info("Snib saved! sp {0}".format(request.data["especievalidabusqueda"]))
            except Exception as e:
                logger.error(e)
                return Response({
                                    'status':'error',
                                    'message': str(e),
                                    'idejemplar':request.data["idejemplar"],
                                }, status.HTTP_500_INTERNAL_SERVER_ERROR)
            return Response({
                                'status': 'success',
                                'message':'snib row inserted!',
                                'idejemplar':snib.idejemplar,
                            }, status.HTTP_201_CREATED)
        return Response({
                            'status': 'error',
                            'message':'no valid data',
                            'idejemplar':request.data["idejemplar"]
                        }, status.HTTP_400_BAD_REQUEST)

    def get_serializer(self):
        return SnibSerializer()

class MultiSnibView(APIView):
    """
    Servicio que inserta multiples registros de *snib*, que como en el servicio `insertSnib` inserta las respectivas filas en las tablas *sp_snib*, *isp_snib* y *geo_snib*, apartir de un json con multiples datos que representan filas de tipo *snib*.
    """
    def post(self, request):
        amount_of_time = 0
        number_of_rows = len(request.data["data"])
        responses = []
        for i in range(number_of_rows):
            try:
                request.data["data"][i]["altitudmapa"] = int(request.data["data"][i]["altitudmapa"])
            except:
                pass
        mserial = MultipleSnibSerializer(data=request.data)
        if len(request.data["data"]) == 0:
            return Response({"responses":responses}, status.HTTP_200_OK)
        mserial = MultipleSnibSerializer(data=request.data)
        if mserial.is_valid():
            try:
                mserial.save()
            except Exception as e:
                logger.error(str(e))
                responses.append({'status': 'error',
                                    'message': str(e),
                                })
            return Response({"responses":responses}, status.HTTP_200_OK)
        else:
            Response({'status': 'error',
                        'message':'no valid data after modify existing rows'}, status.HTTP_500_INTERNAL_SERVER_ERROR)

    def get_serializer(self):
        return MultipleSnibSerializer()

class GetInfoSpecie(APIView):
    """
    Obtiene la informacion taxonomica de una especie dado el nombre cientifico de la especie o el numbre comun
    """
    def post(self, request):
        try:
            if request.data['name_specie'] != None:
                name_specie = request.data['name_specie']
            else:
                pass
        except Exception as e:
            logger.error(str(e))
            return Response({'data':{}}, status.HTTP_400_BAD_REQUEST)
