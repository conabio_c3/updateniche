from django.apps import AppConfig


class UpdatedbConfig(AppConfig):
    name = 'updatedb'
