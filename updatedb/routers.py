import os

class Niche2Router(object):
    """A router to control all database operations on models in the myapp2 application"""

    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'updatedb':
            return os.environ['DBNAME1']
        return 'default'

    def db_for_write(self, model, **hints):
        if model._meta.app_label == 'updatedb':
            return os.environ['DBNAME1']
        return 'default'

    def allow_relation(self, obj1, obj2, **hints):
        if obj1._meta.app_label == 'updatedb' or obj2._meta.app_label == 'updatedb':
            return True
        return True