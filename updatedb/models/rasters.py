from django.contrib.gis.db import models
from django.contrib.postgres import fields

class RasterBins(models.Model):
    tag = models.CharField(max_length=100, blank=True, null=True)
    label = models.CharField(max_length=200, blank=True, null=True)
    layer = models.CharField(max_length=100, blank=True, null=True)
    icat = models.IntegerField(default=True, null=False) 
    bid = models.AutoField(primary_key=True)
    cells_64km = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    cells_32km = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    cells_16km = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    cells_8km = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)

    class Meta:
        db_table = 'raster_bins'
        indexes = []