from django.contrib.gis.db import models
from .grid import *

class GeoSnib(models.Model):
    longitud = models.FloatField(blank=True, null=True)
    latitud = models.FloatField(blank=True, null=True)
    geoid = models.AutoField(primary_key=True)
    the_geom = models.GeometryField(srid=4326)
    geom_m = models.PointField(srid=900913)
    gridid_64km = models.IntegerField(blank=False, null=False)
    gridid_32km = models.IntegerField(blank=False, null=False)
    gridid_16km = models.IntegerField(blank=False, null=False)
    gridid_8km  = models.IntegerField(blank=False, null=False)

    class Meta:
        db_table = 'geo_snib'
        indexes = [
            models.Index(fields=['geoid'], name='idx_geo_snib_geoid'),
        ]
