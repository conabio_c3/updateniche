from django.contrib.gis.db import models

class Aoi(models.Model):
    gid = models.AutoField(primary_key=True)
    fgid = models.IntegerField(blank=False, null=False)
    geom = models.GeometryField(blank=True, null=True)
    country =  models.CharField(max_length=200, blank=True, null=True)
    
    class Meta:
        db_table = "aoi"

class America(models.Model):
	gid = models.AutoField(primary_key=True)
	geom = models.GeometryField(blank=True, null=True)
	country =  models.CharField(max_length=200, blank=True, null=True)

	class Meta:
		db_table = "america"

class CotaAmerica(models.Model):
	gid = models.AutoField(primary_key=True)
	id_c = models.IntegerField(db_column='id',blank=True, null=True)
	geom = models.GeometryField(blank=True, null=True)

	class Meta:
		db_table = "cota_america"

		
		