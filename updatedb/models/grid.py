from django.contrib.gis.db import models
from django.contrib.postgres import fields


class Grid64Km(models.Model):
    gcol = models.IntegerField(blank=True, null=True)
    grow = models.IntegerField(blank=True, null=True)
    geom = models.GeometryField(blank=True, null=True)
    gridid_64km = models.IntegerField(primary_key=True)
    the_geom = models.GeometryField(blank=True, null=True)
    small_geom = models.GeometryField(blank=True, null=True)

    class Meta:
        db_table = 'grid_64km'
        indexes = [
            models.Index(fields=['gridid_64km'],
                name='idx_grid_64km_gridid_64km'),
            ]

class Grid64KmAoi(models.Model):
    gcol = models.IntegerField(blank=True, null=True)
    grow = models.IntegerField(blank=True, null=True)
    gridid_64km = models.IntegerField(primary_key=True)
    geom = models.GeometryField(blank=True, null=True)
    the_geom = models.GeometryField(blank=True, null=True)
    small_geom = models.GeometryField(blank=True, null=True)
    plantae = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    animalia = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    fungi = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    protoctista = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    prokaryotae = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)

    class Meta:
        db_table = 'grid_64km_aoi'


class Grid32KmAoi(models.Model):
    gcol = models.IntegerField(blank=True, null=True)
    grow = models.IntegerField(blank=True, null=True)
    gridid_64km = models.IntegerField(blank=True, null=True)
    gridid_32km = models.AutoField(primary_key=True)
    geom = models.GeometryField(blank=True, null=True)
    the_geom = models.GeometryField(blank=True, null=True)
    small_geom = models.GeometryField(blank=True, null=True)
    plantae = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    animalia = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    fungi = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    protoctista = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    prokaryotae = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)

    class Meta:
        db_table = 'grid_32km_aoi'

class Grid16KmAoi(models.Model):
    gcol = models.IntegerField(blank=True, null=True)
    grow = models.IntegerField(blank=True, null=True)
    gridid_64km = models.IntegerField(blank=True, null=True)
    gridid_32km = models.IntegerField(blank=True, null=True)
    gridid_16km = models.AutoField(primary_key=True)
    geom = models.GeometryField(blank=True, null=True)
    the_geom = models.GeometryField(blank=True, null=True)
    small_geom = models.GeometryField(blank=True, null=True)
    plantae = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    animalia = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    fungi = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    protoctista = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    prokaryotae = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)

    class Meta:
        db_table = 'grid_16km_aoi'

class Grid8KmAoi(models.Model):
    gcol = models.IntegerField(blank=True, null=True)
    grow = models.IntegerField(blank=True, null=True)
    gridid_64km = models.IntegerField(blank=False, null=False)
    gridid_32km = models.IntegerField(blank=False, null=False)
    gridid_16km = models.IntegerField(blank=False, null=False)
    gridid_8km = models.AutoField(primary_key=True)
    geom = models.GeometryField(blank=True, null=True)
    small_geom = models.GeometryField(blank=True, null=True)
    the_geom = models.GeometryField(blank=True, null=True)
    plantae = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    animalia = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    fungi = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    protoctista = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    prokaryotae = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)


    class Meta:
        db_table = 'grid_8km_aoi'
        indexes = [
            models.Index(fields=['gridid_8km'], name='idx_grid_8km_aoi_gridid_8km'),
        ]
