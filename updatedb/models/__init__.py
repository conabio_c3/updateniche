from django.contrib.gis.db import models
from django.contrib.postgres.operations import CreateExtension
from django.db import migrations

from .geosnib import GeoSnib
from .grid import (Grid64Km,
                  Grid64KmAoi,
                  Grid32KmAoi,
                  Grid16KmAoi,
                  Grid8KmAoi)
from .occurrence import Snib
from .species import (SpSnib,
                     IspSnib)


class Migration(migrations.Migration):
    operations = [
        CreateExtension('postgis'),
    ]


class ValidaTerceros(models.Model):
    nombre = models.CharField(max_length=100, null=False)
    url = models.CharField(max_length=400, null=False)

    class Meta:
        db_table = "valida_terceros"
