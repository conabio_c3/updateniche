from django.contrib.gis.db import models
from django.contrib.postgres import fields


class SpSnib(models.Model):
    reinovalido = models.CharField(max_length=100, blank=True, null=True)
    phylumdivisionvalido = models.CharField(max_length=100, blank=True,
            null=True)
    clasevalida = models.CharField(max_length=100, blank=True, null=True)
    ordenvalido = models.CharField(max_length=100, blank=True, null=True)
    familiavalida = models.CharField(max_length=100, blank=True, null=True)
    generovalido = models.CharField(max_length=100, blank=True, null=True)
    especievalidabusqueda = models.CharField(max_length=200, blank=True, null=True)
    spid = models.AutoField(primary_key=True)
    validadoterceros = models.IntegerField(default=True, null=False)
    cells_64km = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    cells_32km = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    cells_16km = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    cells_8km = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)

    class Meta:
        db_table = 'sp_snib'
        indexes = [
            models.Index(fields=['spid'], name='idx_sp_snib_spid'),
            models.Index(fields=['especievalidabusqueda', 'reinovalido', 'phylumdivisionvalido'], name='idx_sp_snib_spphykin'),
            ]


class IspSnib(models.Model):
    reinovalido = models.CharField(max_length=100, blank=True, null=True)
    phylumdivisionvalido = models.CharField(max_length=100, blank=True, null=True)
    clasevalida = models.CharField(max_length=100, blank=True, null=True)
    ordenvalido = models.CharField(max_length=100, blank=True, null=True)
    familiavalida = models.CharField(max_length=100, blank=True, null=True)
    generovalido = models.CharField(max_length=100, blank=True, null=True)
    especievalidabusqueda = models.CharField(max_length=200, blank=True, null=True)
    especievalida = models.CharField(max_length=200, blank=True, null=True)
    categoriainfraespecievalida = models.CharField(max_length=40, blank=True, null=True)
    spid = models.ForeignKey(SpSnib, on_delete=models.CASCADE, db_column='spid')
    ispid = models.AutoField(primary_key=True)
    cells_64km = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    cells_32km = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    cells_16km = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)
    cells_8km = fields.ArrayField(base_field=models.IntegerField(),
            blank=True,
            null=True)

    class Meta:
        db_table = 'isp_snib'
        indexes = [
            models.Index(fields=['ispid'], name='idx_isp_snib_ispid'),
            models.Index(fields=['spid'], name='idx_isp_snib_spid'),
            models.Index(fields=['reinovalido','phylumdivisionvalido','clasevalida','ordenvalido',
                                'familiavalida','generovalido','especievalidabusqueda','especievalida',
                                'categoriainfraespecievalida'], name='idx_isp_snib_all'),
        ]

