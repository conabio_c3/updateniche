import json
import logging
import time
from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework.test import APIRequestFactory
from rest_framework.test import APIClient
from django.contrib.gis.geos import GEOSGeometry, Point
from django.db import connections
from time import sleep
import pytest
from .models import *
from . import views
from .auxiliar_functions.sp_snib import *
from .auxiliar_functions.countries import *

class TestClass(APITestCase):

    multi_db = True
    logger = logging.getLogger(__name__)

    def insert_snib_spe_ex_geo_ex(self):
        """ Inserta un nuevo registro en la tabla snib con especie existente y
            localizacion existente
        """
        self.logger.info(">>> INICIA TEST: insert_snib_spe_ex_geo_ex")
        no_locations_before = GeoSnib.objects.count()
        no_specimens_before = Snib.objects.count()
        no_species_before = SpSnib.objects.count()

        url = reverse('multinsertsnib')
        with open('data/spe_ex_geo_ex.json', 'r', encoding="utf-8") as f:
            data = json.load(f)
        response = self.client.post(url,  data, format='json')
        no_locations_after = GeoSnib.objects.count()
        no_specimens_after = Snib.objects.count()
        no_species_after = SpSnib.objects.count()

        self.assertEqual(no_locations_before, no_locations_after)
        self.assertEqual(no_specimens_before + 1, no_specimens_after)
        self.assertEqual(no_species_before, no_species_after)

        self.assertEqual(response.status_code, 200)

    def insert_snib_spe_ex_geo_new(self):
        """ Inserta un nuevo registro en la tabla snib con una especie
            existente pero nueva localizacion
        """
        self.logger.info(">>> INICIA TEST: insert_snib_spe_ex_geo_new")
        no_locations_before = GeoSnib.objects.count()
        no_specimens_before = Snib.objects.count()
        no_species_before = SpSnib.objects.count()

        url = reverse('multinsertsnib')
        with open('data/spe_ex_geo_new.json', 'r', encoding="utf-8") as f:
            data = json.load(f)
        response = self.client.post(url, data, format='json')

        no_locations_after = GeoSnib.objects.count()
        no_specimens_after = Snib.objects.count()
        no_species_after = SpSnib.objects.count()

        self.assertEqual(no_locations_before + 1, no_locations_after)
        self.assertEqual(no_specimens_before + 1, no_specimens_after)
        self.assertEqual(no_species_before, no_species_before)

        gs_agregado = GeoSnib.objects.last()
        self.assertEqual(gs_agregado.longitud, data["data"][0]['longitud'])
        self.assertEqual(gs_agregado.latitud, data["data"][0]['latitud'])

        self.assertEqual(response.status_code, 200)

    def insert_snib_spe_new_geo_new(self):
        """ Inserta un nuevo registro en la tabla snib con una especie y
            localizacion nueva
        """
        self.logger.info(">>> INICIA TEST: insert_snib_spe_new_geo_new")
        no_locations_before = GeoSnib.objects.count()
        no_specimens_before = Snib.objects.count()
        no_species_before = SpSnib.objects.count()

        url = reverse('multinsertsnib')
        with open('data/spe_new_geo_new.json', 'r', encoding="utf-8") as f:
            data = json.load(f)
        response = self.client.post(url, data, format='json')

        no_locations_after = GeoSnib.objects.count()
        no_specimens_after = Snib.objects.count()
        no_species_after = SpSnib.objects.count()

        self.assertEqual(no_locations_before + 1, no_locations_after)
        self.assertEqual(no_specimens_before + 1, no_specimens_after)
        self.assertEqual(no_species_before + 1, no_species_after)

        gs_agregado = GeoSnib.objects.last()
        self.assertEqual(gs_agregado.longitud, data["data"][0]['longitud'])
        self.assertEqual(gs_agregado.latitud, data["data"][0]['latitud'])

        sp_agregada = SpSnib.objects.last()
        self.assertEqual(sp_agregada.reinovalido, data["data"][0]['reinovalido'])
        self.assertEqual(sp_agregada.phylumdivisionvalido,
                         data["data"][0]['phylumdivisionvalido'])
        self.assertEqual(sp_agregada.clasevalida, data["data"][0]['clasevalida'])
        self.assertEqual(sp_agregada.ordenvalido, data["data"][0]['ordenvalido'])
        self.assertEqual(sp_agregada.familiavalida, data["data"][0]['familiavalida'])
        self.assertEqual(sp_agregada.generovalido, data["data"][0]['generovalido'])
        self.assertEqual(sp_agregada.especievalidabusqueda,
                         data["data"][0]['especievalidabusqueda'])

        # self.assertEqual(sp_agregada.validadoterceros, 1)
        self.assertEqual(response.status_code, 200)

    def insert_snib_spe_new_geo_ex(self):
        """ Inserta un nuevo registro en la tabla snib con una especie nueva y
            una localizacion existente
        """
        self.logger.info(">>> INICIA TEST: insert_snib_spe_new_geo_ex")
        no_locations_before = GeoSnib.objects.count()
        no_specimens_before = Snib.objects.count()
        no_species_before = SpSnib.objects.count()

        url = reverse('multinsertsnib')
        with open('data/spe_new_geo_ex.json', 'r', encoding="utf-8") as f:
            data = json.load(f)
        response = self.client.post(url, data, format='json')

        no_locations_after = GeoSnib.objects.count()
        no_specimens_after = Snib.objects.count()
        no_species_after = SpSnib.objects.count()

        self.assertEqual(no_locations_before, no_locations_after)
        self.assertEqual(no_specimens_before + 1, no_specimens_after)
        self.assertEqual(no_species_before + 1, no_species_after)

        sp_agregada = SpSnib.objects.last()
        self.assertEqual(sp_agregada.reinovalido, data["data"][0]['reinovalido'])
        self.assertEqual(sp_agregada.phylumdivisionvalido,
                         data["data"][0]['phylumdivisionvalido'])
        self.assertEqual(sp_agregada.clasevalida, data["data"][0]['clasevalida'])
        self.assertEqual(sp_agregada.ordenvalido, data["data"][0]['ordenvalido'])
        self.assertEqual(sp_agregada.familiavalida, data["data"][0]['familiavalida'])
        self.assertEqual(sp_agregada.generovalido, data["data"][0]['generovalido'])
        self.assertEqual(sp_agregada.especievalidabusqueda,
                         data["data"][0]['especievalidabusqueda'])

        self.assertEqual(sp_agregada.validadoterceros, 0)
        self.assertEqual(response.status_code, 200)

    def batch_insert(self):
        """ Prueba de insercion en batch
        """
        self.logger.info(">>> INICIA TEST: batch_insert")
        no_locations_before = GeoSnib.objects.count()
        no_specimens_before = Snib.objects.count()
        no_species_before = SpSnib.objects.count()
        self.logger.info("No. geo_snib al inicio: {}".format(no_locations_before))
        self.logger.info("No. snib al inicio: {}".format(no_specimens_before))
        self.logger.info("No. sp_snib al inicio: {}".format(no_species_before))

        url = reverse('multinsertsnib')
        with open('data/batch_insert.json', 'r', encoding="utf-8") as f:
            data = json.load(f)
        response = self.client.post(url, data, format='json')

        no_locations_after = GeoSnib.objects.count()
        no_specimens_after = Snib.objects.count()
        no_species_after = SpSnib.objects.count()
        self.logger.info("No. geo_snib al final: {}".format(no_locations_after))
        self.logger.info("No. snib al final: {}".format(no_specimens_after))
        self.logger.info("No. sp_snib al final: {}".format(no_species_after))

        # comprobacion de respuesta
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_specimens_before + 10, no_specimens_after)

    def medium_batch_insert(self):
        """ Prueba de insercion en batch mediano
        """
        self.logger.info(">>> INICIA TEST: medium_batch_insert")
        no_locations_before = GeoSnib.objects.count()
        no_specimens_before = Snib.objects.count()
        no_species_before = SpSnib.objects.count()
        self.logger.info("No. geo_snib al inicio: {}".format(no_locations_before))
        self.logger.info("No. snib al inicio: {}".format(no_specimens_before))
        self.logger.info("No. sp_snib al inicio: {}".format(no_species_before))

        url = reverse('multinsertsnib')
        with open('data/medium_batch_insert.json', 'r', encoding="utf-8") as f:
            data = json.load(f)
        response = self.client.post(url, data, format='json')

        no_locations_after = GeoSnib.objects.count()
        no_specimens_after = Snib.objects.count()
        no_species_after = SpSnib.objects.count()
        self.logger.info("No. geo_snib al final: {}".format(no_locations_after))
        self.logger.info("No. sp_snib al final: {}".format(no_species_after))
        self.logger.info("No. snib al final: {}".format(no_specimens_after))

        self.assertEqual(response.status_code, 200)

    def spe_ex_update_grid_lists(self):
        """ Prueba para comprobar la actualizacion de las listas de grids en las
            tablas sp_snib e isp_snib
        """
        self.logger.info(">>> INICIA TEST: spe_ex_update_grid_lists")
        with open('data/spe_ex_geo_new.json', 'r', encoding="utf-8") as f:
            data = json.load(f)
        url = reverse('multinsertsnib')
        response = self.client.post(url, data, format='json')

        sp = SpSnib.objects.get(especievalidabusqueda=data["data"][0]["especievalidabusqueda"])
        isp = IspSnib.objects.get(especievalidabusqueda=data["data"][0]["especievalidabusqueda"])

        geo = GeoSnib.objects.get(the_geom=Point(data["data"][0]["longitud"], data["data"][0]["latitud"], srid=4326))

        self.assertIn(geo.gridid_64km, sp.cells_64km)
        self.assertIn(geo.gridid_32km, sp.cells_32km)
        self.assertIn(geo.gridid_16km, sp.cells_16km)
        self.assertIn(geo.gridid_8km, sp.cells_8km)

        self.assertIn(geo.gridid_64km, isp.cells_64km)
        self.assertIn(geo.gridid_32km, isp.cells_32km)
        self.assertIn(geo.gridid_16km, isp.cells_16km)
        self.assertIn(geo.gridid_8km, isp.cells_8km)

    def insert_snib_spe_new_update_grids(self):
        """ Actualiza columnas de reinos en las tablas de grids de diferentes tamanos 
            al insertar un nuevo snib
        """
        self.logger.info(">>> INICIA TEST: insert_snib_spe_new_update_grids")
        url = reverse('multinsertsnib')
        with open('data/spe_new_geo_ex.json', 'r', encoding="utf-8") as f:
            data = json.load(f)
        response = self.client.post(url, data, format='json')
        
        sp_agregada = SpSnib.objects.last()
        with connections['niche2'].cursor() as cursor:
            sql = ("SELECT 1 FROM grid_64km_aoi "
                   "WHERE gridid_64km = {gridid} AND {spid} = ANY(plantae)")
            cursor.execute(sql.format(gridid = sp_agregada.cells_64km[0],
                spid=sp_agregada.spid))
            info_en_grid = cursor.fetchall()

        self.assertEqual(response.status_code, 200)
        self.assertTrue(info_en_grid)

        with connections['niche2'].cursor() as cursor:
            sql = ("SELECT 1 FROM grid_32km_aoi "
                   "WHERE gridid_32km = {gridid} AND {spid} = ANY(plantae)")
            cursor.execute(sql.format(gridid = sp_agregada.cells_32km[0],
                spid=sp_agregada.spid))
            info_en_grid = cursor.fetchall()

        self.assertTrue(info_en_grid)

        with connections['niche2'].cursor() as cursor:
            sql = ("SELECT 1 FROM grid_16km_aoi "
                   "WHERE gridid_16km = {gridid} AND {spid} = ANY(plantae)")
            cursor.execute(sql.format(gridid = sp_agregada.cells_16km[0],
                spid=sp_agregada.spid))
            info_en_grid = cursor.fetchall()

        self.assertTrue(info_en_grid)

        with connections['niche2'].cursor() as cursor:
            sql = ("SELECT 1 FROM grid_8km_aoi "
                   "WHERE gridid_8km = {gridid} AND {spid} = ANY(plantae)")
            cursor.execute(sql.format(gridid = sp_agregada.cells_8km[0],
                spid=sp_agregada.spid))
            info_en_grid = cursor.fetchall()

        self.assertTrue(info_en_grid)

    def not_changed_data(self):
        """ Comprueba el manejo de registros de snib que ya existan cuando se no
            se corrige ningun cambio
        """
        self.logger.info(">>> INICIA TEST: not_changed_data")
        no_locations_before = GeoSnib.objects.count()
        no_specimens_before = Snib.objects.count()
        no_species_before = SpSnib.objects.count()

        url = reverse('multinsertsnib')
        with open('data/not_changed_data.json', 'r', encoding="utf-8") as f:
            data = json.load(f)

        snib_before = Snib.objects.get(idejemplar=data["data"][0]["idejemplar"])

        response = self.client.post(url, data, format='json')

        no_locations_after = GeoSnib.objects.count()
        no_specimens_after = Snib.objects.count()
        no_species_after = SpSnib.objects.count()

        snib_after = Snib.objects.get(idejemplar=data["data"][0]["idejemplar"])

        self.assertEqual(snib_before, snib_after)
        self.assertEqual(no_locations_before, no_locations_after)
        self.assertEqual(no_specimens_before, no_specimens_after)
        self.assertEqual(no_species_before, no_species_before)
        self.assertEqual(response.status_code, 200)

    def changed_data(self):
        """ Comprueba el manejo de registros de snib que ya existan cuando se
            corrige un cambio
        """
        self.logger.info(">>> INICIA TEST: changed_data")
        no_locations_before = GeoSnib.objects.count()
        no_specimens_before = Snib.objects.count()
        no_species_before = SpSnib.objects.count()

        url = reverse('multinsertsnib')
        with open('data/changed_data.json', 'r', encoding="utf-8") as f:
            data = json.load(f)

        snib_before = Snib.objects.get(idejemplar=data["data"][0]["idejemplar"])

        response = self.client.post(url, data, format='json')

        no_locations_after = GeoSnib.objects.count()
        no_specimens_after = Snib.objects.count()
        no_species_after = SpSnib.objects.count()

        snib_updated = Snib.objects.get(idejemplar="15cd405b5e51da565a60bfd423c04d5f")

        self.assertEqual(snib_before.especie, "Xylophanes zurcheri")
        self.assertEqual(snib_updated.especie, "Xylophanes zurchery")
        self.assertEqual(no_locations_before, no_locations_after)
        self.assertEqual(no_specimens_before, no_specimens_after)
        self.assertEqual(no_species_before, no_species_before)
        self.assertEqual(response.status_code, 200)

    def batch_insert_eu(self):
        """ Prueba de insercion en batch de 500 registros gbif transformados a snib
        """
        self.logger.info(">>> INICIA TEST: batch_insert_eu")
        no_locations_before = GeoSnib.objects.count()
        no_specimens_before = Snib.objects.count()
        no_species_before = SpSnib.objects.count()

        self.logger.info("No. geo_snib al inicio: {}".format(no_locations_before))
        self.logger.info("No. snib al inicio: {}".format(no_specimens_before))
        self.logger.info("No. sp_snib al inicio: {}".format(no_species_before))

        url = reverse('multinsertsnib')
        with open('data/batch_eu_2.json', 'r', encoding="utf-8") as f:
            data = json.load(f)

        species_to_insert = {d['especievalidabusqueda'] for d in data["data"]}
        geosnibs_to_insert = []

        for d in data["data"]:
            new_point = Point(d['longitud'], d['latitud'], srid=4326)
            exist = False
            for p in geosnibs_to_insert:
                if p == new_point:
                    exist = True
                    continue
            if not exist:
                geosnibs_to_insert.append(new_point)

        ispecies_to_insert = {d['especievalidabusqueda'] for d in data["data"]}

        num_new_species = 0
        num_new_geo = 0
        num_new_ispecies = 0

        for specie in species_to_insert:
            try:
                SpSnib.objects.get(especievalidabusqueda=specie)
            except:
                num_new_species += 1

        for geo in geosnibs_to_insert:
            try:
                GeoSnib.objects.get(the_geom=geo)
            except:
                num_new_geo += 1

        for ispecie in ispecies_to_insert:
            try:
                IspSnib.objects.get(especievalidabusqueda=ispecie)
            except:
                num_new_ispecies += 1

        response = self.client.post(url, data, format='json')

        no_locations_after = GeoSnib.objects.count()
        no_specimens_after = Snib.objects.count()
        no_species_after = SpSnib.objects.count()

        self.logger.info("No. geo_snib al final: {}".format(no_locations_after))
        self.logger.info("No. sp_snib al final: {}".format(no_species_after))
        self.logger.info("No. snib al final: {}".format(no_specimens_after))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_locations_before + num_new_geo, no_locations_after)
        self.assertEqual(no_specimens_before + 500, no_specimens_after)
        self.assertEqual(no_species_before + num_new_species, no_species_after)

    def batch_insert_alaska(self):
        """ Prueba de insercion en batch de 500 registros gbif transformados a snib. En este json de 500 
            registros de snib hay 412 con ubicacion en Alaska.
        """
        self.logger.info(">>> INICIA TEST: batch_insert_eu_alaska")
        no_locations_before = GeoSnib.objects.count()
        no_specimens_before = Snib.objects.count()
        no_species_before = SpSnib.objects.count()

        self.logger.info("No. geo_snib al inicio: {}".format(no_locations_before))
        self.logger.info("No. snib al inicio: {}".format(no_specimens_before))
        self.logger.info("No. sp_snib al inicio: {}".format(no_species_before))

        url = reverse('multinsertsnib')
        with open('data/batch_eu_alaska.json', 'r', encoding="utf-8") as f:
            data = json.load(f)

        species_to_insert = {d['especievalidabusqueda'] for d in data["data"]}
        geosnibs_to_insert = []

        for d in data["data"]:
            new_point = GeoSnib(longitud=d['longitud'], latitud=d['latitud'])
            exist = False
            for p in geosnibs_to_insert:
                if p.longitud == new_point.longitud and p.latitud == new_point.latitud:
                    exist = True
                    continue
            if not exist:
                geosnibs_to_insert.append(new_point)

        ispecies_to_insert = {d['especievalidabusqueda'] for d in data["data"]}

        num_new_species = 0
        num_new_geo = 0
        num_new_ispecies = 0

        for specie in species_to_insert:
            try:
                SpSnib.objects.get(especievalidabusqueda=specie)
            except SpSnib.DoesNotExist:
                num_new_species += 1

        for geo in geosnibs_to_insert:
            try:
                GeoSnib.objects.get(longitud=geo.longitud, latitud=geo.latitud)
            except GeoSnib.DoesNotExist:
                num_new_geo += 1

        for ispecie in ispecies_to_insert:
            try:
                IspSnib.objects.get(especievalidabusqueda=ispecie)
            except IspSnib.DoesNotExist:
                num_new_ispecies += 1

        response = self.client.post(url, data, format='json')
        
        no_locations_after = GeoSnib.objects.count()
        no_specimens_after = Snib.objects.count()
        no_species_after = SpSnib.objects.count()

        self.logger.info("No. geo_snib al final: {}".format(no_locations_after))
        self.logger.info("No. sp_snib al final: {}".format(no_species_after))
        self.logger.info("No. snib al final: {}".format(no_specimens_after))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_specimens_before + 88, no_specimens_after)
        self.assertEqual(no_species_before + num_new_species, no_species_after)

    def Craniata_Chordata(self):
        """ Prueba para verificar si se corrige Craniata - Chrodata 
        """
        self.logger.info(">>> INICIA TEST: Craniata_Chordata")

        url = reverse('multinsertsnib')
        with open('data/Lynx_rufus_Craniata.json', 'r', encoding="utf-8") as f:
            data = json.load(f)

        response = self.client.post(url, data, format='json')

        old_spid_lr = SpSnib.objects.last().spid

        with open('data/Lynx_rufus_Chordata.json', 'r', encoding="utf-8") as f:
            data = json.load(f)

        response = self.client.post(url, data, format='json')

        new_spid_lr = SpSnib.objects.last().spid

        correct_craniata()

        num_lr_chordata = SpSnib.objects.filter(especievalidabusqueda="Lynx rufus").filter(phylumdivisionvalido="Chordata").count()
        num_lr_craniata = SpSnib.objects.filter(especievalidabusqueda="Lynx rufus").filter(phylumdivisionvalido="Craniata").count()
        
        old_snib = Snib.objects.filter(especievalidabusqueda="Lynx rufus").filter(spid=old_spid_lr)
        new_snib = Snib.objects.filter(especievalidabusqueda="Lynx rufus").filter(spid=new_spid_lr)

        num_old_snib = old_snib.count()
        num_new_snib = new_snib.count()

        self.assertEqual(num_lr_chordata, 1)
        self.assertEqual(num_lr_craniata, 0)
        self.assertEqual(num_old_snib, 2)
        self.assertEqual(num_new_snib, 0)

        lassnib = Snib.objects.last()

        grid8km  = Grid8KmAoi.objects.get(gridid_8km=lassnib.gridid_8km)
        grid16km = Grid16KmAoi.objects.get(gridid_16km=lassnib.gridid_16km)
        grid32km = Grid32KmAoi.objects.get(gridid_32km=lassnib.gridid_32km)
        grid64km = Grid64KmAoi.objects.get(gridid_64km=lassnib.gridid_64km)

        self.assertIn(old_spid_lr, grid8km.animalia)
        self.assertIn(old_spid_lr, grid16km.animalia)
        self.assertIn(old_spid_lr, grid32km.animalia)
        self.assertIn(old_spid_lr, grid64km.animalia)

    def craniata_chordata_2(self):
        """ Prueba para verificar si se corrige Craniata - Chrodata 
        """
        sp_craniata =  get_sp_craniata()
        sp_craniata_list = [sp.especievalidabusqueda for sp in sp_craniata]
        logger.info(sp_craniata_list)

        n = len(sp_craniata)

        sp_list = [SpSnib.objects.get(especievalidabusqueda=sp_craniata_list[i], phylumdivisionvalido="Craniata") for i in range(n)]

        spids  = [sp_list[i].spid for i in range(n)]
        spids2 = [sp.spid for sp in sp_craniata]

        sp_cells64_list = [sp_list[i].cells_64km + sp_craniata[i].cells_64km for i in range(n)] 
        sp_cells32_list = [sp_list[i].cells_32km + sp_craniata[i].cells_32km for i in range(n)]
        sp_cells16_list = [sp_list[i].cells_16km + sp_craniata[i].cells_16km for i in range(n)]
        sp_cells8_list  = [sp_list[i].cells_8km  + sp_craniata[i].cells_8km  for i in range(n)]

        sp_cells64_list_chor = [sp_craniata[i].cells_64km for i in range(n)] 
        sp_cells32_list_chor = [sp_craniata[i].cells_32km for i in range(n)]
        sp_cells16_list_chor = [sp_craniata[i].cells_16km for i in range(n)]
        sp_cells8_list_chor  = [sp_craniata[i].cells_8km  for i in range(n)]

        correct_craniata()

        for i in range(n):
            print(sp_craniata_list[i])
            self.assertEqual(SpSnib.objects.filter(especievalidabusqueda=sp_craniata_list[i], phylumdivisionvalido="Craniata").count(), 0)
            self.assertEqual(SpSnib.objects.filter(especievalidabusqueda=sp_craniata_list[i], phylumdivisionvalido="Chordata").count(), 1)

            self.assertEqual(SpSnib.objects.get(especievalidabusqueda=sp_craniata_list[i], phylumdivisionvalido="Chordata").cells_64km, sp_cells64_list[i])
            self.assertEqual(SpSnib.objects.get(especievalidabusqueda=sp_craniata_list[i], phylumdivisionvalido="Chordata").cells_32km, sp_cells32_list[i])
            self.assertEqual(SpSnib.objects.get(especievalidabusqueda=sp_craniata_list[i], phylumdivisionvalido="Chordata").cells_16km, sp_cells16_list[i])
            self.assertEqual(SpSnib.objects.get(especievalidabusqueda=sp_craniata_list[i], phylumdivisionvalido="Chordata").cells_8km, sp_cells8_list[i])

            self.assertEqual(SpSnib.objects.get(spid=spids[i]).phylumdivisionvalido, "Chordata")

            self.assertEqual(Snib.objects.filter(spid=spids2[i]).count(), 0)

            for j in range(len(sp_cells64_list_chor[i])):
                self.assertIn(spids[i], Grid64KmAoi.objects.get(gridid_64km=sp_cells64_list_chor[i][j]).animalia)

            for j in range(len(sp_cells32_list_chor[i])):
                self.assertIn(spids[i], Grid32KmAoi.objects.get(gridid_32km=sp_cells32_list_chor[i][j]).animalia)

            for j in range(len(sp_cells16_list_chor[i])):
                self.assertIn(spids[i], Grid16KmAoi.objects.get(gridid_16km=sp_cells16_list_chor[i][j]).animalia)

            for j in range(len(sp_cells8_list_chor[i])):
                self.assertIn(spids[i], Grid8KmAoi.objects.get(gridid_8km=sp_cells8_list_chor[i][j]).animalia)

    def correct_geo_multiple_returned(self):
        """ Esto es para probar mecanismo para evitar duplicacion de geosnib 
            durante la insercion   
        """
        self.logger.info(">>> INICIA TEST: correct_geo_multiple_returned")
        url = reverse('multinsertsnib')

        with open('./data/spe_new_geo_new.json', 'r') as f:
           data1 = json.load(f)

        with open('./data/spe_new_geo_ex.json', 'r') as f:
           data2 = json.load(f)

        idejemplar1 = data1['data'][0]['idejemplar']
        idejemplar2 = data2['data'][0]['idejemplar']
        response = self.client.post(url, data1, format='json')

        geosnib_data={'longitud':-86.9032288, 'latitud':20.8449657}                 
        geosnib = GeoSnib()
        geosnib.longitud=geosnib_data['longitud']
        geosnib.latitud=geosnib_data['latitud']
        geosnib.the_geom = GEOSGeometry('Point({0} {1})'.format(geosnib_data['longitud'], geosnib_data['latitud']), srid=4326)
        geosnib.save()

        geoid = geosnib.geoid
        #self.logger.info(geoid)
        data2['data'][0]['longitud'] = -86.9032288
        data2['data'][0]['latitud'] = 20.8449657

        response = self.client.post(url, data2, format='json')

        snib1 = Snib.objects.get(idejemplar=idejemplar1)
        snib2 = Snib.objects.get(idejemplar=idejemplar2)

        self.assertEqual(snib1.geoid, snib2.geoid)
        
        numpoint = GeoSnib.objects.filter(**geosnib_data).count()
        self.assertEqual(numpoint, 1)


    def correct_isp_multiple_returned(self):
        """ Esto es para probar mecanismo para evitar duplicacion de isp_snib 
            durante la insercion   
        """
        self.logger.info(">>> INICIA TEST: correct_isp_multiple_returned")
        url = reverse('multinsertsnib')

        with open('./data/spe_ex_geo_new.json', 'r') as f:
           data1 = json.load(f)

        with open('./data/spe_new_geo_ex.json', 'r') as f:
           data2 = json.load(f)

        idejemplar1 = data1['data'][0]['idejemplar']
        idejemplar2 = data2['data'][0]['idejemplar']
        response = self.client.post(url, data1, format='json')

        ispsnib_data={'reinovalido':data1['data'][0]['reinovalido'],
                'phylumdivisionvalido':data1['data'][0]['phylumdivisionvalido'],
                'clasevalida':data1['data'][0]['clasevalida'],
                'ordenvalido':data1['data'][0]['ordenvalido'],
                'familiavalida':data1['data'][0]['familiavalida'],
                'generovalido':data1['data'][0]['generovalido'],
                'especievalidabusqueda':data1['data'][0]['especievalidabusqueda'],
                'especievalida':data1['data'][0]['especievalida'],
                'categoriainfraespecievalida':data1['data'][0]['categoriainfraespecievalida']}                 
        ispsnib = IspSnib(**ispsnib_data)
        ispsnib.save()

        ispid=ispsnib.ispid
        #self.logger.info(ispid)
        data2['data'][0]['reinovalido']=ispsnib_data['reinovalido']
        data2['data'][0]['phylumdivisionvalido']=ispsnib_data['phylumdivisionvalido']
        data2['data'][0]['clasevalida']=ispsnib_data['clasevalida']
        data2['data'][0]['ordenvalido']=ispsnib_data['ordenvalido']
        data2['data'][0]['familiavalida']=ispsnib_data['familiavalida']
        data2['data'][0]['generovalido']=ispsnib_data['generovalido']
        data2['data'][0]['especievalidabusqueda']=ispsnib_data['especievalidabusqueda']
        data2['data'][0]['especievalida']=ispsnib_data['especievalida']
        data2['data'][0]['categoriainfraespecievalida']=ispsnib_data['categoriainfraespecievalida']

        response = self.client.post(url, data2, format='json')

        snib1 = Snib.objects.get(idejemplar=idejemplar1)
        snib2 = Snib.objects.get(idejemplar=idejemplar2)

        self.assertEqual(snib1.ispid, snib2.ispid)
        
        numpoint = IspSnib.objects.filter(**ispsnib_data).count()
        self.assertEqual(numpoint, 1)

    def update_fields(self):
        """
        Test para  probar metodo que cambia campos en un registro de snib
        """
        url = reverse('multinsertsnib')
        with open('./data/updatesnib.json', 'r') as f:
            data = json.load(f)

        response = self.client.post(url, data, format='json')

        self.logger.info(">>> INICIA TEST: update fields geosnib case 1")

        with open('./data/updatesnibgeo_1.json', 'r') as f:
            data = json.load(f)

        snib = Snib.objects.get(idejemplar=data['data'][0]['idejemplar']) 
        old_data = {'longitud':snib.longitud, 'latitud': snib.latitud, 'the_geom': snib.the_geom,
                    'gridid_8km':snib.gridid_8km, 'gridid_16km':snib.gridid_16km, 'gridid_32km':snib.gridid_32km,
                    'gridid_64km':snib.gridid_64km, 'geoid': snib.geoid.geoid}

        response = self.client.post(url, data, format='json')

        snib = Snib.objects.get(idejemplar=data['data'][0]['idejemplar'])

        new_data = {'longitud':snib.longitud, 'latitud': snib.latitud, 'the_geom': snib.the_geom,
                    'gridid_8km':snib.gridid_8km, 'gridid_16km':snib.gridid_16km, 'gridid_32km':snib.gridid_32km,
                    'gridid_64km':snib.gridid_64km, 'geoid': snib.geoid.geoid}            

        logger.info("old_data " + str(old_data))
        logger.info("new_data " + str(new_data))

        self.assertNotEqual(old_data['geoid'], snib.geoid.geoid)
        self.assertNotEqual(old_data, new_data)

        self.logger.info(">>> INICIA TEST: update fields geosnib case 2")

        with open('./data/updatesnibgeo_2.json', 'r') as f:
            data = json.load(f)

        num_geo_before = GeoSnib.objects.count()
        logger.info("There are {0} geo_snib rows!".format(num_geo_before))

        snib = Snib.objects.get(idejemplar=data['data'][0]['idejemplar']) 
        old_data = {'longitud':snib.longitud, 'latitud': snib.latitud, 'the_geom': snib.the_geom,
                    'gridid_8km':snib.gridid_8km, 'gridid_16km':snib.gridid_16km, 'gridid_32km':snib.gridid_32km,
                    'gridid_64km':snib.gridid_64km, 'geoid': snib.geoid.geoid}

        response = self.client.post(url, data, format='json')

        snib = Snib.objects.get(idejemplar=data['data'][0]['idejemplar'])

        new_data = {'longitud':snib.longitud, 'latitud': snib.latitud, 'the_geom': snib.the_geom,
                    'gridid_8km':snib.gridid_8km, 'gridid_16km':snib.gridid_16km, 'gridid_32km':snib.gridid_32km,
                    'gridid_64km':snib.gridid_64km, 'geoid': snib.geoid.geoid}            

        logger.info("old_data " + str(old_data))
        logger.info("new_data " + str(new_data))

        try:
            gs = GeoSnib.objects.get(longitud=data['data'][0]['longitud'], latitud=data['data'][0]['latitud'])
            self.assertTrue()
        except:
            pass

        num_geo_after = GeoSnib.objects.count()
        logger.info("There are {0} geo_snib rows!".format(num_geo_after))

        self.assertNotEqual(old_data['geoid'], snib.geoid.geoid)
        self.assertNotEqual(old_data, new_data)
        self.assertEqual(num_geo_before + 1, num_geo_after)

        self.logger.info(">>> INICIA TEST: update fields spsnib case 1")

        with open('./data/updatesnibsp_1.json', 'r') as f:
            data = json.load(f)

        snib = Snib.objects.get(idejemplar=data['data'][0]['idejemplar']) 
        old_data = {'especievalidabusqueda':snib.especievalidabusqueda, 'phylumdivisionvalido':snib.phylumdivisionvalido,
                    'reinovalido':snib.reinovalido, 'spid':snib.spid.spid}

        response = self.client.post(url, data, format='json')

        snib = Snib.objects.get(idejemplar=data['data'][0]['idejemplar'])

        new_data = {'especievalidabusqueda':snib.especievalidabusqueda, 'phylumdivisionvalido':snib.phylumdivisionvalido,
                    'reinovalido':snib.reinovalido, 'spid':snib.spid.spid}            

        logger.info("old_data " + str(old_data))
        logger.info("new_data " + str(new_data))

        self.assertNotEqual(old_data['spid'], snib.spid.spid)
        self.assertNotEqual(old_data, new_data)

        self.logger.info(">>> INICIA TEST: update fields spsnib case 2")

        with open('./data/updatesnibsp_2.json', 'r') as f:
            data = json.load(f)

        num_sp_before = SpSnib.objects.count()
        logger.info("There are {0} species".format(num_sp_before))

        snib = Snib.objects.get(idejemplar=data['data'][0]['idejemplar']) 
        old_data = {'especievalidabusqueda':snib.especievalidabusqueda, 'phylumdivisionvalido':snib.phylumdivisionvalido,
                    'reinovalido':snib.reinovalido, 'spid':snib.spid.spid}

        response = self.client.post(url, data, format='json')

        num_sp_after = SpSnib.objects.count()
        logger.info("There are {0} species".format(num_sp_after))

        snib = Snib.objects.get(idejemplar=data['data'][0]['idejemplar'])

        new_data = {'especievalidabusqueda':snib.especievalidabusqueda, 'phylumdivisionvalido':snib.phylumdivisionvalido,
                    'reinovalido':snib.reinovalido, 'spid':snib.spid.spid}            

        logger.info("old_data " + str(old_data))
        logger.info("new_data " + str(new_data))
        self.assertNotEqual(old_data['spid'], snib.spid.spid)
        self.assertNotEqual(old_data, new_data)
        self.assertEqual(num_sp_before + 1, num_sp_after)

    def correct_gid(self):
        """
        Test para  probar que se agregue correctamente el gid
        """
        self.logger.info(">>> INICIA TEST: correct gid")

        url = reverse('multinsertsnib')

        with open('./data/updatesnib.json', 'r') as f:
            data = json.load(f)

        response = self.client.post(url, data, format='json')

        snib = Snib.objects.get(idejemplar=data['data'][0]['idejemplar'])

        self.assertEqual(snib.gid.gid, 253)

    def test_add_country(self):
        """
        Test para agregar country a aoi
        """
        self.logger.info(">>> INICIA TEST: add american contry")

        countries_num_before = Aoi.objects.count()
        cells_num_64km_before = Grid64KmAoi.objects.count()
        cells_num_32km_before = Grid32KmAoi.objects.count()
        cells_num_16km_before = Grid16KmAoi.objects.count()
        cells_num_8km_before = Grid8KmAoi.objects.count()
        self.logger.info("countries' number before method {0}".format(countries_num_before))
        self.logger.info("cells of 64km number before method {0}".format(cells_num_64km_before))
        self.logger.info("cells of 32km number before method {0}".format(cells_num_32km_before))
        self.logger.info("cells of 16km number before method {0}".format(cells_num_16km_before))
        self.logger.info("cells of 8km number before method {0}".format(cells_num_8km_before))
        #country = 'MEXICO'
        #country = 'VENEZUELA'
        country='ECUADOR'
        american_countries = get_american_countries()
        for obj in american_countries:
            if obj['country'] == country:
                #print(obj)
                gid = obj['gid']
                break
        add_american_country(gid)
        add_american_country(gid)
        new_country = Aoi.objects.last()
        countries_num_after = Aoi.objects.count()
        cota_america_after = CotaAmerica.objects.count()
        cells_num_64km_after = Grid64KmAoi.objects.count()
        cells_num_32km_after = Grid32KmAoi.objects.count()
        cells_num_16km_after = Grid16KmAoi.objects.count()
        cells_num_8km_after = Grid8KmAoi.objects.count()
        self.logger.info("new country {0} with gid {1}".format(new_country.country, new_country.gid))
        self.logger.info("countries' number after method {0}".format(countries_num_after))
        self.logger.info("cotas number after method {0}".format(cota_america_after))
        self.logger.info("cells of 64km number after method {0}".format(cells_num_64km_after))
        self.logger.info("cells of 32km number after method {0}".format(cells_num_32km_after))
        self.logger.info("cells of 16km number after method {0}".format(cells_num_16km_after))
        self.logger.info("cells of 8km number after method {0}".format(cells_num_8km_after))
        self.assertEqual(countries_num_after, countries_num_before + 1)




