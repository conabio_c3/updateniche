from .models import *
from django.db.models import F
from celery.decorators import task
from celery import group
from updateNiche.celery import app
from itertools import islice
import multiprocessing
import logging
from .external_validation.snib import *
from .external_validation.sp_snib import *
from rest_framework import serializers
from django.contrib.gis.geos import GEOSGeometry, Point
from .models import (GeoSnib,
                     IspSnib,
                     SpSnib,
                     Snib,
                     Grid8KmAoi,
                     Grid16KmAoi,
                     Grid32KmAoi,
                     Grid64KmAoi)
import multiprocessing as mp
from .auxiliar_functions.snib import *
from .auxiliar_functions.sp_snib import *
from .auxiliar_functions.isp_snib import *
from .auxiliar_functions.geosnib import *
from .auxiliar_functions.grid import *
from django.db.models import Q

num_cores = multiprocessing.cpu_count()
length_spid = int(500/num_cores)

def chunks(it, n):
    for first in it:
        yield [first] + list(islice(it, n - 1))

def chunks_dict(d, it, n):
    return ({key : d[key] for key in k} for k in chunks(it, n))

@app.task
def update_grids_8km(spid_dict_8km_animalia,
                      spid_dict_8km_plantae,
                      spid_dict_8km_fungi,
                      spid_dict_8km_protoctista,
                      spid_dict_8km_prokaryotae):
    try:
        res = True
        res = res and update_grid8km_animalia(spid_dict_8km_animalia)
        res = res and update_grid8km_plantae(spid_dict_8km_plantae)
        res = res and update_grid8km_fungi(spid_dict_8km_fungi)
        res = res and update_grid8km_protoctista(spid_dict_8km_protoctista)
        res = res and update_grid8km_prokaryotae(spid_dict_8km_prokaryotae)
    except Exception as e:
        print(str(e))
        return False
    return res

@app.task
def update_grids_16km(spid_dict_16km_animalia,
                      spid_dict_16km_plantae,
                      spid_dict_16km_fungi,
                      spid_dict_16km_protoctista,
                      spid_dict_16km_prokaryotae):
    try:
        res = True
        res = res and update_grid16km_animalia(spid_dict_16km_animalia)
        res = res and update_grid16km_plantae(spid_dict_16km_plantae)
        res = res and update_grid16km_fungi(spid_dict_16km_fungi)
        res = res and update_grid16km_protoctista(spid_dict_16km_protoctista)
        res = res and update_grid16km_prokaryotae(spid_dict_16km_prokaryotae)
    except Exception as e:
        print(str(e))
        return False
    return res

@app.task
def update_grids_32km(spid_dict_32km_animalia,
                      spid_dict_32km_plantae,
                      spid_dict_32km_fungi,
                      spid_dict_32km_protoctista,
                      spid_dict_32km_prokaryotae):
    try:
        res = True
        res = res and update_grid32km_animalia(spid_dict_32km_animalia)
        res = res and update_grid32km_plantae(spid_dict_32km_plantae)
        res = res and update_grid32km_fungi(spid_dict_32km_fungi)
        res = res and update_grid32km_protoctista(spid_dict_32km_protoctista)
        res = res and update_grid32km_prokaryotae(spid_dict_32km_prokaryotae)
    except Exception as e:
        print(str(e))
        return False
    return res

@app.task
def update_grids_64km(spid_dict_64km_animalia,
                      spid_dict_64km_plantae,
                      spid_dict_64km_fungi,
                      spid_dict_64km_protoctista,
                      spid_dict_64km_prokaryotae):
    try:
        res = True
        res = res and update_grid64km_animalia(spid_dict_64km_animalia)
        res = res and update_grid64km_plantae(spid_dict_64km_plantae)
        res = res and update_grid64km_fungi(spid_dict_64km_fungi)
        res = res and update_grid64km_protoctista(spid_dict_64km_protoctista)
        res = res and update_grid64km_prokaryotae(spid_dict_64km_prokaryotae)
    except Exception as e:
        print(str(e))
        return False
    return res

@app.task
def update_grids_animalia(spid_dict_8km_animalia ,
                          spid_dict_16km_animalia ,
                          spid_dict_32km_animalia ,
                          spid_dict_64km_animalia):
    try:
        res = True
        res = res and update_grid8km_animalia(spid_dict_8km_animalia)  
        res = res and update_grid16km_animalia(spid_dict_16km_animalia)
        res = res and update_grid32km_animalia(spid_dict_32km_animalia)
        res = res and update_grid64km_animalia(spid_dict_64km_animalia)
    except Exception as e:
        print(str(e))
        return False
    return res

def update_grid8km_animalia(spid_dict_8km_animalia):
    try:
        for key in spid_dict_8km_animalia:
            aux = list(set(spid_dict_8km_animalia[str(key)]))
            grid_8km = Grid8KmAoi.objects.filter(gridid_8km__in=aux)
            grid_8km.update(animalia=F('animalia')+[int(key)] if not key is F('animalia') else F('animalia'))
    except Exception as e:
        print(str(e))
        return False
    return True

def update_grid16km_animalia(spid_dict_16km_animalia):
    try:
        for key in spid_dict_16km_animalia:
            aux = list(set(spid_dict_16km_animalia[str(key)]))
            grid_16km = Grid16KmAoi.objects.filter(gridid_16km__in=aux)
            grid_16km.update(animalia=F('animalia')+[int(key)] if not key is F('animalia') else F('animalia'))
    except Exception as e:
        print(str(e))
        return False
    return True

def update_grid32km_animalia(spid_dict_32km_animalia):
    try:
        for key in spid_dict_32km_animalia:
            aux  =list(set(spid_dict_32km_animalia[str(key)]))
            grid_32km = Grid32KmAoi.objects.filter(gridid_32km__in=aux)
            grid_32km.update(animalia=F('animalia')+[int(key)] if not key is F('animalia') else F('animalia'))
    except Exception as e:
        print(str(e))
        return False
    return True

def update_grid64km_animalia(spid_dict_64km_animalia):
    try:
        for key in spid_dict_64km_animalia:
            aux = list(set(spid_dict_64km_animalia[str(key)]))
            grid_64km = Grid64KmAoi.objects.filter(gridid_64km__in=aux)
            grid_64km.update(animalia=F('animalia')+[int(key)] if not key is F('animalia') else F('animalia'))
    except Exception as e:
        print(str(e))
        return False
    return True

@app.task
def update_grids_plantae(spid_dict_8km_plantae ,
                         spid_dict_16km_plantae ,
                         spid_dict_32km_plantae ,
                         spid_dict_64km_plantae):
    try:
        res = True 
        res = res and update_grid8km_plantae(spid_dict_8km_plantae) 
        res = res and update_grid16km_plantae(spid_dict_16km_plantae) 
        res = res and update_grid32km_plantae(spid_dict_32km_plantae)
        res = res and update_grid64km_plantae(spid_dict_64km_plantae)
    except Exception as e:
        print(str(e))
        return False
    return res

def update_grid8km_plantae(spid_dict_8km_plantae):
    try:
        for key in spid_dict_8km_plantae:
            aux = list(set(spid_dict_8km_plantae[str(key)]))
            grid_8km = Grid8KmAoi.objects.filter(gridid_8km__in=aux)
            grid_8km.update(plantae=F('plantae')+[int(key)] if not key is F('plantae') else F('plantae'))
    except Exception as e:
        print(str(e))
        return False
    return True

def update_grid16km_plantae(spid_dict_16km_plantae):
    try:
        for key in spid_dict_16km_plantae:
            aux = list(set(spid_dict_16km_plantae[str(key)]))
            grid_16km = Grid16KmAoi.objects.filter(gridid_16km__in=aux)
            grid_16km.update(plantae=F('plantae')+[int(key)] if not key is F('plantae') else F('plantae'))
    except Exception as e:
        print(str(e))
        return False
    return True

def update_grid32km_plantae(spid_dict_32km_plantae):
    try:
        for key in spid_dict_32km_plantae:
            aux = list(set(spid_dict_32km_plantae[str(key)]))
            grid_32km = Grid32KmAoi.objects.filter(gridid_32km__in=aux)
            grid_32km.update(plantae=F('plantae')+[int(key)] if not key is F('plantae') else F('plantae'))
    except Exception as e:
        print(str(e))
        return False
    return True

def update_grid64km_plantae(spid_dict_64km_plantae):
    try:
        for key in spid_dict_64km_plantae:
            aux = list(set(spid_dict_64km_plantae[str(key)]))
            grid_64km = Grid64KmAoi.objects.filter(gridid_64km__in=aux)
            grid_64km.update(plantae=F('plantae')+[int(key)] if not key is F('plantae') else F('plantae'))
    except Exception as e:
        print(str(e))
        return False
    return True

@app.task
def update_grids_fungi(spid_dict_8km_fungi,
                       spid_dict_16km_fungi,
                       spid_dict_32km_fungi,
                       spid_dict_64km_fungi):
    try:
        res = True
        res = res and update_grid8km_fungi(spid_dict_8km_fungi)
        res = res and update_grid16km_fungi(spid_dict_16km_fungi)
        res = res and update_grid32km_fungi(spid_dict_32km_fungi)
        res = res and update_grid64km_fungi(spid_dict_64km_fungi)
    except Exception as e:
        print(str(e))
        return False
    return res

def update_grid8km_fungi(spid_dict_8km_fungi):
    try:
        for key in spid_dict_8km_fungi:
            aux = list(set(spid_dict_8km_fungi[str(key)]))
            grid_8km = Grid8KmAoi.objects.filter(gridid_8km__in=aux)
            grid_8km.update(fungi=F('fungi')+[int(key)] if not key is F('fungi') else F('fungi'))
    except Exception as e:
        print(str(e))
        return False
    return True

def update_grid16km_fungi(spid_dict_16km_fungi):
    try:
        for key in spid_dict_16km_fungi:
            aux = list(set(spid_dict_16km_fungi[str(key)]))
            grid_16km = Grid16KmAoi.objects.filter(gridid_16km__in=aux)
            grid_16km.update(fungi=F('fungi')+[int(key)] if not key is F('fungi') else F('fungi'))
    except Exception as e:
        print(str(e))
        return False
    return True

def update_grid32km_fungi(spid_dict_32km_fungi):
    try:
        for key in spid_dict_32km_fungi:
            aux = list(set(spid_dict_32km_fungi[str(key)]))
            grid_32km = Grid32KmAoi.objects.filter(gridid_32km__in=aux)
            grid_32km.update(fungi=F('fungi')+[int(key)] if not key is F('fungi') else F('fungi'))
    except Exception as e:
        print(str(e))
        return False
    return True

def update_grid64km_fungi(spid_dict_64km_fungi):
    try:
        for key in spid_dict_64km_fungi:
            aux = list(set(spid_dict_64km_fungi[str(key)]))
            grid_64km = Grid64KmAoi.objects.filter(gridid_64km__in=aux)
            grid_64km.update(fungi=F('fungi')+[int(key)] if not key is F('fungi') else F('fungi'))
    except Exception as e:
        print(str(e))
        return False
    return True        

@app.task
def update_grids_protoctista(spid_dict_8km_protoctista ,
                             spid_dict_16km_protoctista ,
                             spid_dict_32km_protoctista ,
                             spid_dict_64km_protoctista):
    try:
        res = True 
        res = res and update_grid8km_protoctista(spid_dict_8km_protoctista)
        res = res and update_grid16km_protoctista(spid_dict_16km_protoctista) 
        res = res and update_grid32km_protoctista(spid_dict_32km_protoctista)   
        res = res and update_grid64km_protoctista(spid_dict_64km_protoctista)
    except Exception as e:
        print(str(e))
        return False
    return res

def update_grid8km_protoctista(spid_dict_8km_protoctista):
    try:
        for key in spid_dict_8km_protoctista:
            aux = list(set(spid_dict_8kmFalse_protoctista[str(key)]))
            grid_8km = Grid8KmAoi.objects.filter(gridid_8km__in=aux)
            grid_8km.update(protoctista=F('protoctista')+[int(key)] if not key is F('protoctista') else F('protoctista'))
    except Exception as e:
        print(str(e))
        return False
    return True

def update_grid16km_protoctista(spid_dict_16km_protoctista):
    try:
        for key in spid_dict_16km_protoctista:
            grid_16km = Grid16KmAoi.objects.filter(gridid_16km__in=set(spid_dict_16km_protoctista[str(key)]))
            grid_16km.update(protoctista=F('protoctista')+[int(key)] if not key is F('protoctista') else F('protoctista'))
    except Exception as e:
        print(str(e))
        return False
    return True

def update_grid32km_protoctista(spid_dict_32km_protoctista):
    try:
        for key in spid_dict_32km_protoctista:
            aux = list(set(spid_dict_32km_protoctista[str(key)]))
            grid_32km = Grid32KmAoi.objects.filter(gridid_32km__in=aux)
            grid_32km.update(protoctista=F('protoctista')+[int(key)] if not key is F('protoctista') else F('protoctista'))
    except Exception as e:
        print(str(e))
        return False
    return TrueFalse

def update_grid64km_protoctista(spid_dict_64km_protoctista):
    try:
        for key in spid_dict_64km_protoctista:
            aux = list(set(spid_dict_64km_protoctista[str(key)]))
            grid_64km = Grid64KmAoi.objects.filter(gridid_64km__in=aux)
            grid_64km.update(protoctista=F('protoctista')+[int(key)] if not key is F('protoctista') else F('protoctista'))
    except Exception as e:
        print(str(e))
        return False
    return True       

@app.task
def update_grids_prokaryotae(spid_dict_8km_prokaryotae ,
                             spid_dict_16km_prokaryotae ,
                             spid_dict_32km_prokaryotae ,
                             spid_dict_64km_prokaryotae):
    try:
        res = True 
        res = res and update_grid8km_prokaryotae(spid_dict_8km_prokaryotae)
        res = res and update_grid16km_prokaryotae(spid_dict_16km_prokaryotae)
        res = res and update_grid32km_prokaryotae(spid_dict_32km_prokaryotae)
        res = res and update_grid64km_prokaryotae(spid_dict_64km_prokaryotae) 
    except Exception as e:
        print(str(e))
        return False
    return res

@app.task
def update_grid8km_prokaryotae(spid_dict_8km_prokaryotae):
    try:
        for key in spid_dict_8km_prokaryotae:
            aux = list(set(spid_dict_8km_prokaryotae[str(key)]))
            grid_8km = Grid8KmAoi.objects.filter(gridid_8km__in=aux)
            grid_8km.update(prokaryotae=F('prokaryotae')+[int(key)] if not key is F('prokaryotae') else F('prokaryotae'))
    except Exception as e:
        print(str(e))
        return False
    return True

def update_grid16km_prokaryotae(spid_dict_16km_prokaryotae):
    try:
        for key in spid_dict_16km_prokaryotae:
            aux = list(set(spid_dict_16km_prokaryotae[str(key)]))
            grid_16km = Grid16KmAoi.objects.filter(gridid_16km__in=aux)
            grid_16km.update(prokaryotae=F('prokaryotae')+[int(key)] if not key is F('prokaryotae') else F('prokaryotae'))
    except Exception as e:
        print(str(e))
        return False
    return True

def update_grid32km_prokaryotae(spid_dict_32km_prokaryotae):
    try:
        for key in spid_dict_32km_prokaryotae:
            aux = list(set(spid_dict_32km_prokaryotae[str(key)]))
            grid_32km = Grid32KmAoi.objects.filter(gridid_32km__in=aux)
            grid_32km.update(prokaryotae=F('prokaryotae')+[int(key)] if not key is F('prokaryotae') else F('prokaryotae'))
    except Exception as e:
        print(str(e))
        return False
    return True

def update_grid64km_prokaryotae(spid_dict_64km_prokaryotae):
    try:
        for key in spid_dict_64km_prokaryotae:
            aux = list(set(spid_dict_64km_prokaryotae[str(key)]))
            grid_64km = Grid64KmAoi.objects.filter(gridid_64km__in=aux)
            grid_64km.update(prokaryotae=F('prokaryotae')+[int(key)] if not key is F('prokaryotae') else F('prokaryotae'))
    except Exception as e:
        print(str(e))
        return False
    return True

@app.task
def valida_specie(especievalidabusqueda, phylumdivisionvalido, reinovalido, spid):
    try:
        validation_num = validate(especievalidabusqueda, phylumdivisionvalido, reinovalido)
        SpSnib.objects.filter(spid=spid).update(validadoterceros=validation_num)
    except Exception as e:
        print(str(e))
        return False
    return True

def validate_all_species(l):
    res = group(valida_specie.s(chunk[0], chunk[1], chunk[2], chunk[3])
                       for chunk in chunks(iter(l), 4)).apply_async() 
    return res
         
@app.task
def save_multisnib(validate_data):
    number_of_rows = len(validate_data["data"])
    idejemplar_to_drop = []
    for i in range(number_of_rows):
        try:
            snib = Snib.objects.get(idejemplar=validate_data["data"][i]["idejemplar"])
            if not compare_snib_fields(snib, validate_data["data"][i]):
                try:
                    snib = update_snib_fields(snib, validate_data["data"][i])
                    if snib != None:
                        snib.save()
                        logger.info("Fields of {0} updated".format(snib.idejemplar))
                    else:
                        snib = Snib.objects.get(idejemplar=validate_data["data"][i]["idejemplar"])
                        snib.delete()
                        logger.info("snib with ID {0} deleted!".format(snib.idejemplar))    
                except Exception as e:
                    logger.info(str(e))
            else:
                logger.info("snib row with ID {0} already exist and the request don't have new data".format(snib.idejemplar))
            idejemplar_to_drop.append(snib.idejemplar)    
        except Snib.DoesNotExist:
            pass
    validate_data["data"] = [d for d in validate_data["data"] if not d["idejemplar"] in idejemplar_to_drop]
    snib_to_insert = []
    sp_to_update = []
    isp_to_update = []
    geo_to_insert = []
    num_to_insert = len(validate_data["data"])
    indexes_with_error = []
    for i in range(num_to_insert):
        snib_to_insert.append(Snib(**validate_data["data"][i]))

    spsnib_to_validate = []

    for i in range(num_to_insert):
        snib_to_insert[i].the_geom = GEOSGeometry('Point({0} {1})'.format(snib_to_insert[i].longitud, snib_to_insert[i].latitud), srid=4326)
        point900913 = GEOSGeometry('Point({0} {1})'.format(snib_to_insert[i].longitud, snib_to_insert[i].latitud), srid=4326).transform(900913)
        snib_to_insert[i].geom_m = point900913
        try:
            intersection = Grid8KmAoi.objects.get(the_geom__intersects=snib_to_insert[i].the_geom)
        except Exception as e:
            logger.warn("{0} for snib {1}".format(str(e), snib_to_insert[i].idejemplar))
            indexes_with_error.append(i)
            continue

        try:
            gr = Grid64KmAoi.objects.get(gridid_64km=intersection.gridid_64km)
            country = Aoi.objects.filter(geom__intersects=gr.the_geom)[0]
            snib_to_insert[i].gid = country
            logger.info("La ocurrencia {0} de {1}".format(snib_to_insert[i].idejemplar, country.country))
        except Exception as e:
            logger.warn("{0} for snib {1}".format(str(e), snib_to_insert[i].idejemplar))
            indexes_with_error.append(i)
            continue

        snib_to_insert[i].gridid_64km = intersection.gridid_64km
        snib_to_insert[i].gridid_32km = intersection.gridid_32km
        snib_to_insert[i].gridid_16km = intersection.gridid_16km
        snib_to_insert[i].gridid_8km  = intersection.gridid_8km
        geosnib_data = geo_data(snib_to_insert[i])
        ispsnib_data = isp_data(snib_to_insert[i])
        spsnib_data = sp_data(snib_to_insert[i])

        try:
            spsnib = SpSnib.objects.get(Q(especievalidabusqueda=spsnib_data["especievalidabusqueda"]) &
                                        Q(phylumdivisionvalido=spsnib_data["phylumdivisionvalido"]) &
                                        Q(reinovalido=spsnib_data["reinovalido"]))
            inserted_sp = False
        except SpSnib.DoesNotExist:
            # check synonyms
            inserted_sp = True
            logger.info("Getting synonyms of specie {0}".format(spsnib_data["especievalidabusqueda"]))
            synonyms = has_synonyms(spsnib_data["especievalidabusqueda"])
            #print(synonyms)
            #print(snib_to_insert[i].especievalidabusqueda)
            for synonym in synonyms:
                try:
                    spsnib = SpSnib.objects.get(especievalidabusqueda=synonym)
                    snib_to_insert[i].especievalidabusqueda = spsnib.especievalidabusqueda
                    inserted_sp = False
                    break
                except:
                    logger.info("synonym {0} of specie {1} isn't recorded".format(snib_to_insert[i].especievalidabusqueda, synonym))
        except SpSnib.MultipleObjectsReturned:
            correct_multiples_sp_returned(spsnib_data)
            spsnib = SpSnib.objects.get(Q(especievalidabusqueda=spsnib_data["especievalidabusqueda"]) &
                                        Q(phylumdivisionvalido=spsnib_data["phylumdivisionvalido"]) &
                                        Q(reinovalido=spsnib_data["reinovalido"]))
            inserted_sp = False
                

        try:
            ispsnib = IspSnib.objects.get(Q(reinovalido=ispsnib_data['reinovalido']) &
                                          Q(phylumdivisionvalido=ispsnib_data['phylumdivisionvalido']) &
                                          Q(clasevalida=ispsnib_data['clasevalida']) &
                                          Q(ordenvalido=ispsnib_data['ordenvalido']) &
                                          Q(familiavalida=ispsnib_data['familiavalida']) &
                                          Q(generovalido=ispsnib_data['generovalido']) &
                                          Q(especievalidabusqueda=ispsnib_data['especievalidabusqueda']) &
                                          Q(especievalida=ispsnib_data['especievalida']) &
                                          Q(categoriainfraespecievalida=ispsnib_data['categoriainfraespecievalida']))
            inserted_isp = False
        except IspSnib.DoesNotExist:
            inserted_isp = True      
        except IspSnib.MultipleObjectsReturned:
            correct_multiples_isp_returned(ispsnib_data)
            ispsnib = IspSnib.objects.get(Q(reinovalido=ispsnib_data['reinovalido']) &
                                          Q(phylumdivisionvalido=ispsnib_data['phylumdivisionvalido']) &
                                          Q(clasevalida=ispsnib_data['clasevalida']) &
                                          Q(ordenvalido=ispsnib_data['ordenvalido']) &
                                          Q(familiavalida=ispsnib_data['familiavalida']) &
                                          Q(generovalido=ispsnib_data['generovalido']) &
                                          Q(especievalidabusqueda=ispsnib_data['especievalidabusqueda']) &
                                          Q(especievalida=ispsnib_data['especievalida']) &
                                          Q(categoriainfraespecievalida=ispsnib_data['categoriainfraespecievalida']))
            inserted_isp = False

        try:
            geosnib = GeoSnib.objects.get(the_geom=snib_to_insert[i].the_geom)
            inserted_geo = False
        except GeoSnib.DoesNotExist:
            inserted_geo = True
        except GeoSnib.MultipleObjectsReturned:
            correct_multiples_geo_returned(geosnib_data)
            geosnib = GeoSnib.objects.get(the_geom=snib_to_insert[i].the_geom)
            inserted_geo = False
            

        if inserted_sp:
            spsnib = SpSnib(**spsnib_data)
            spsnib.cells_64km = [intersection.gridid_64km]
            spsnib.cells_32km = [intersection.gridid_32km]
            spsnib.cells_16km = [intersection.gridid_16km]
            spsnib.cells_8km  = [intersection.gridid_8km]
        else :
            spsnib.cells_64km.append(intersection.gridid_64km)
            spsnib.cells_64km = list(set(spsnib.cells_64km))
            spsnib.cells_32km.append(intersection.gridid_32km)
            spsnib.cells_32km = list(set(spsnib.cells_32km))
            spsnib.cells_16km.append(intersection.gridid_16km)
            spsnib.cells_16km = list(set(spsnib.cells_16km))
            spsnib.cells_8km.append(intersection.gridid_8km)
            spsnib.cells_8km  = list(set(spsnib.cells_8km))
        spsnib.save()

        if inserted_isp:
            ispsnib = IspSnib(**ispsnib_data)
            ispsnib.spid = spsnib
            ispsnib.cells_64km = [intersection.gridid_64km]
            ispsnib.cells_32km = [intersection.gridid_32km]
            ispsnib.cells_16km = [intersection.gridid_16km]
            ispsnib.cells_8km  = [intersection.gridid_8km]
        else :
            ispsnib.cells_64km.append(intersection.gridid_64km)
            ispsnib.cells_64km = list(set(ispsnib.cells_64km))
            ispsnib.cells_32km.append(intersection.gridid_32km)
            ispsnib.cells_32km = list(set(ispsnib.cells_32km))
            ispsnib.cells_16km.append(intersection.gridid_16km)
            ispsnib.cells_16km = list(set(ispsnib.cells_16km))
            ispsnib.cells_8km.append(intersection.gridid_8km)
            ispsnib.cells_8km = list(set(ispsnib.cells_8km))
        ispsnib.save()

        if inserted_geo:
            geosnib = GeoSnib(**geosnib_data)
            point900913 = snib_to_insert[i].the_geom
            point900913.transform(900913)
            geosnib.geom_m = point900913
            geosnib.the_geom = snib_to_insert[i].the_geom
            geosnib.gridid_64km = intersection.gridid_64km
            geosnib.gridid_32km = intersection.gridid_32km
            geosnib.gridid_16km = intersection.gridid_16km
            geosnib.gridid_8km = intersection.gridid_8km
            geosnib.save()

        snib_to_insert[i].spid = spsnib
        snib_to_insert[i].ispid = ispsnib
        snib_to_insert[i].geoid = geosnib

        spid = spsnib.spid

        if inserted_sp or spsnib.validadoterceros == None or spsnib.validadoterceros == 0:
            logger.info("Validating ({0}, {1}, {2})".format(snib_to_insert[i].especievalidabusqueda, snib_to_insert[i].phylumdivisionvalido, snib_to_insert[i].reinovalido))
            if valida_specie(snib_to_insert[i].especievalidabusqueda, snib_to_insert[i].phylumdivisionvalido, snib_to_insert[i].reinovalido, spsnib.spid):
                logger.info("specie {0} validated".format(snib_to_insert[i].especievalidabusqueda))
            else:
                logger.info("specie {0} not validated".format(snib_to_insert[i].especievalidabusqueda))

        if spsnib.reinovalido == 'Animalia':
            if not spid in intersection.animalia:
                grid16 = Grid16KmAoi.objects.get(gridid_16km=intersection.gridid_16km)
                grid32 = Grid32KmAoi.objects.get(gridid_32km=intersection.gridid_32km)
                grid64 = Grid64KmAoi.objects.get(gridid_64km=intersection.gridid_64km)
                intersection.animalia = intersection.animalia + [spid]
                grid16.animalia = grid16.animalia + [spid]
                grid32.animalia = grid32.animalia + [spid]
                grid64.animalia = grid64.animalia + [spid]
                intersection.save()
                grid16.save()
                grid32.save()
                grid64.save()
        elif spsnib.reinovalido == 'Plantae':
            if not spid in intersection.plantae:
                grid16 = Grid16KmAoi.objects.get(gridid_16km=intersection.gridid_16km)
                grid32 = Grid32KmAoi.objects.get(gridid_32km=intersection.gridid_32km)
                grid64 = Grid64KmAoi.objects.get(gridid_64km=intersection.gridid_64km)
                intersection.plantae = intersection.plantae + [spid]
                grid16.plantae = grid16.plantae + [spid]
                grid32.plantae = grid32.plantae + [spid]
                grid64.plantae = grid64.plantae + [spid]
                intersection.save()
                grid16.save()
                grid32.save()
                grid64.save()
        elif spsnib.reinovalido == 'Fungi':
            if not spid in intersection.fungi:
                grid16 = Grid16KmAoi.objects.get(gridid_16km=intersection.gridid_16km)
                grid32 = Grid32KmAoi.objects.get(gridid_32km=intersection.gridid_32km)
                grid64 = Grid64KmAoi.objects.get(gridid_64km=intersection.gridid_64km)
                intersection.fungi = intersection.fungi + [spid]
                grid16.fungi = grid16.fungi + [spid]
                grid32.fungi = grid32.fungi + [spid]
                grid64.fungi = grid64.fungi + [spid]
                intersection.save()
                grid16.save()
                grid32.save()
                grid64.save()
        elif spsnib.reinovalido == 'Protoctista':
            if not spid in intersection.protoctista:
                grid16 = Grid16KmAoi.objects.get(gridid_16km=intersection.gridid_16km)
                grid32 = Grid32KmAoi.objects.get(gridid_32km=intersection.gridid_32km)
                grid64 = Grid64KmAoi.objects.get(gridid_64km=intersection.gridid_64km)
                intersection.protoctista = intersection.protoctista + [spid]
                grid16.protoctista = grid16.protoctista + [spid]
                grid32.protoctista = grid32.protoctista + [spid]
                grid64.protoctista = grid64.protoctista + [spid]
                intersection.save()
                grid16.save()
                grid32.save()
                grid64.save()
        elif spsnib.reinovalido == 'Prokaryotae':
            if not spid in intersection.prokaryotae:
                grid16 = Grid16KmAoi.objects.get(gridid_16km=intersection.gridid_16km)
                grid32 = Grid32KmAoi.objects.get(gridid_32km=intersection.gridid_32km)
                grid64 = Grid64KmAoi.objects.get(gridid_64km=intersection.gridid_64km)
                intersection.prokaryotae = intersection.prokaryotae + [spid]
                grid16.prokaryotae = grid16.prokaryotae + [spid]
                grid32.prokaryotae = grid32.prokaryotae + [spid]
                grid64.prokaryotae = grid64.prokaryotae + [spid]
                intersection.save()
                grid16.save()
                grid32.save()
                grid64.save()
            
    snib_to_insert = [snib_to_insert[i] for i in range(num_to_insert) if not i in indexes_with_error]
    snib_to_save = Snib.objects.bulk_create(snib_to_insert)
    #validate_all_species(spsnib_to_validate)
    logger.info("{0} snib row(s) was saved !".format(len(snib_to_insert)))
    return spsnib_to_validate

@app.task
def update_specific_ispid(ispid):
    try:
        i_sp = IspSnib.objects.get(ispid=ispid) 
    except Exception as e:
        logger.error("There isn't infraspecie with ispid {0}. Error: {1}".format(ispid, str(e)))
        return False
    occs = Snib.objects.filter(especievalidabusqueda=i_sp.especievalidabusqueda,
                               reinovalido=i_sp.reinovalido,
                               phylumdivisionvalido=i_sp.phylumdivisionvalido,
                               categoriainfraespecievalida=i_sp.categoriainfraespecievalida)
    #noccs = occs.count()
    #logger.info("{0} ocurrences correspond to infraspecie with ispid {1}".format(noccs, ispid))
    try:
        occs.update(ispid=i_sp, spid=i_sp.spid)
    except Exception as e:
        logger.error("The ocurrences corresponding with spid {0} cuouldn't update".format(ispid))
        return False
    return True

@app.task
def update_specific_geos(geoid):
    try:
        geo = GeoSnib.objects.get(geoid=geoid)
    except Exception as e:
        logger.error("There isn't a point with geoid {0}. Error: {1}".format(geoid, str(e)))
        return False
    occs = Snib.objects.filter(longitud=geo.longitud,
                               latitud=geo.latitud)
    #noccs = occs.count()
    #logger.info("{0} ocurrences correspond to infraspecie with geoid {1}".format(noccs, geoid))
    try:
        occs.update(geoid=geo)
    except Exception as e:
        logger.error("The ocurrences corresponding with geoid {0} cuouldn't update".format(geoid))
        return False
    return True

@app.task
def update_gridid_geos(geoid):
    try:
        geo = GeoSnib.objects.get(geoid=geoid)
    except Exception as e:
        logger.error("There isn't a point with geoid {0}. Error: {1}".format(geoid, str(e)))
        return False
    occs = Snib.objects.filter(geoid=geo)
    try:
        occs.update(gridid_64km=geo.gridid_64km, gridid_32km=geo.gridid_32km,
                    gridid_16km=geo.gridid_16km, gridid_8km=geo.gridid_8km)
    except Exception as e:
        logger.error("The ocurrences corresponding with geoid {0} cuouldn't update".format(geoid))
        return False
    return True

@app.task 
def update_cells_ispids(ispid):
    try:
        isp = IspSnib.objects.get(ispid=ispid)
    except Exception as e:
        logger.error("There isn't a point with geoid {0}. Error: {1}".format(ispid, str(e)))
        return False

    cells_64km = list(Snib.objects.filter(ispid=isp).values('gridid_64km').distinct())
    cells_64km = [obj['gridid_64km'] for obj in cells_64km if obj['gridid_64km'] != None]
    
    cells_32km = list(Snib.objects.filter(ispid=isp).values('gridid_32km').distinct())
    cells_32km = [obj['gridid_32km'] for obj in cells_32km if obj['gridid_32km'] != None]
    
    cells_16km = list(Snib.objects.filter(ispid=isp).values('gridid_16km').distinct())
    cells_16km = [obj['gridid_16km'] for obj in cells_16km if obj['gridid_16km'] != None]
    
    cells_8km = list(Snib.objects.filter(ispid=isp).values('gridid_8km').distinct())
    cells_8km = [obj['gridid_8km'] for obj in cells_8km if obj['gridid_8km'] != None]

    isp.save()

@app.task 
def update_cells_spids(spid):
    try:
        sp = SpSnib.objects.get(spid=spid)
    except Exception as e:
        logger.error("There isn't a point with geoid {0}. Error: {1}".format(spid, str(e)))
        return False

    cells_64km = list(Snib.objects.filter(spid=sp).values('gridid_64km').distinct())
    cells_64km = [obj['gridid_64km'] for obj in cells_64km if obj['gridid_64km'] != None]

    cells_32km = list(Snib.objects.filter(spid=sp).values('gridid_32km').distinct())
    cells_32km = [obj['gridid_32km'] for obj in cells_32km if obj['gridid_32km'] != None]

    cells_16km = list(Snib.objects.filter(spid=sp).values('gridid_16km').distinct())
    cells_16km = [obj['gridid_16km'] for obj in cells_16km if obj['gridid_16km'] != None]

    cells_8km = list(Snib.objects.filter(spid=sp).values('gridid_8km').distinct())
    cells_8km = [obj['gridid_8km'] for obj in cells_8km if obj['gridid_8km'] != None]

    sp.save()

@app.task
def update_geom_geos(geoid):
    try:
        geo = GeoSnib.objects.get(geoid=geoid)
    except Exception as e:
        logger.error("There isn't a point with geoid {0}. Error: {1}".format(geoid, str(e)))
        return False
    occs = Snib.objects.filter(geoid=geo)
    try:
        occs.update(the_geom=geo.the_geom)
    except Exception as e:
        logger.error("The ocurrences corresponding with geoid {0} cuouldn't update".format(geoid))
        return False
    return True

