import time
import logging
from django.db import connections
from .external_validation.snib import *
from rest_framework import serializers
from django.contrib.gis.geos import GEOSGeometry, Point
from .models import (GeoSnib,
                     IspSnib,
                     SpSnib,
                     Snib,
                     Grid8KmAoi,
                     Grid16KmAoi,
                     Grid32KmAoi,
                     Grid64KmAoi)
import multiprocessing as mp
from .auxiliar_functions.sp_snib import *
from .auxiliar_functions.isp_snib import *
from .auxiliar_functions.geosnib import *
from .auxiliar_functions.grid import *
from django.db.models import Q
from .tasks import *
import multiprocessing as mp
from celery import group

logger = logging.getLogger(__name__)

class GeoSnibSerializer(serializers.ModelSerializer):
    class Meta:
        model = GeoSnib
        fields = ('longitud', 'latitud')

    def create(self, validate_data):
        grid8 = Grid8KmAoi.objects.all()
        point900913 = Point(validate_data['longitud'],
                validate_data['latitud'],
                srid=4326)
        point900913.transform(900913)
        #validate_data['geom_m'] = point900913
        validate_data['the_geom'] = Point(validate_data['longitud'],
                validate_data['latitud'],
                srid=4326)
        geom = self.find_first_intersection(validate_data['the_geom'], grid8)
        if geom != None:
            validate_data['gridid_64km'] = geom.gridid_64km
            validate_data['gridid_32km'] = geom.gridid_32km
            validate_data['gridid_16km'] = geom.gridid_16km
            validate_data['gridid_8km'] = geom.gridid_8km
        return GeoSnib.objects.create(**validate_data)

class IspSnibSerializer(serializers.Serializer):
    reinovalido = serializers.CharField(max_length=100)
    phylumdivisionvalido = serializers.CharField(max_length=100)
    clasevalida = serializers.CharField(max_length=100)
    ordenvalido = serializers.CharField(max_length=100)
    familiavalida = serializers.CharField(max_length=100)
    generovalido = serializers.CharField(max_length=100)
    especievalidabusqueda = serializers.CharField(max_length=200)
    especievalida = serializers.CharField(max_length=200)
    categoriainfraespecievalida = serializers.CharField(max_length=40)
    spid = serializers.IntegerField()

    def create(self, validate_data):
        snib_list = Snib.objects.all()
        ispsnib, inserted_isp = IspSnib.objects.get_or_create(**validate_data)
        if inserted_isp:
            ispsnib.spid = spsnib.spid
            ispsnib.cells_64km = list({s.gridid_64km for s in snib_list if s.ispid == ispsnib.ispid and s.gridid_64km != None })
            ispsnib.cells_32km = list({s.gridid_32km for s in snib_list if s.ispid == ispsnib.ispid and s.gridid_32km != None })
            ispsnib.cells_16km = list({s.gridid_16km for s in snib_list if s.ispid == ispsnib.ispid and s.gridid_16km != None })
            ispsnib.cells_8km = list({s.gridid_8km for s in snib_list if s.ispid == ispsnib.ispid and s.gridid_8km != None })
            ispsnib.save()
            logger.info('IspSnib inserted !!')
        return ispsnib

class SpSnibSerializer(serializers.Serializer):
    reinovalido = serializers.CharField(max_length=100)
    phylumdivisionvalido = serializers.CharField(max_length=100)
    clasevalida = serializers.CharField(max_length=100)
    ordenvalido = serializers.CharField(max_length=100)
    familiavalida = serializers.CharField(max_length=100)
    generovalido = serializers.CharField(max_length=100)
    especievalidabusqueda = serializers.CharField(max_length=100)
    validacionterceros = serializers.BooleanField(default=True)

    def create(self, validate_data):
        snib_list = Snib.objects.all()
        spsnib, inserted_sp = SpSnib.objects.get_or_create(**validate_data)
        if inserted_sp:
            spsnib.cells_64km = list({s.gridid_64km for s in snib_list if s.spid == spsnib.spid and s.gridid_64km != None })
            spsnib.cells_32km = list({s.gridid_32km for s in snib_list if s.spid == spsnib.spid and s.gridid_32km != None })
            spsnib.cells_16km = list({s.gridid_16km for s in snib_list if s.spid == spsnib.spid and s.gridid_16km != None })
            spsnib.cells_8km = list({s.gridid_8km for s in snib_list if s.spid == spsnib.spid and s.gridid_8km != None })
            spsnib.save()
            logger.info("SpSnib inserted !!")
        return spsnib

class SnibSerializer(serializers.Serializer):
    grupobio = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    subgrupobio = serializers.CharField(allow_blank=True, allow_null=True, max_length=400)
    familiavalida = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    generovalido = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    especievalida = serializers.CharField(allow_blank=True, allow_null=True, max_length=200)
    nom059 = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    cites = serializers.CharField(allow_blank=True, allow_null=True, )
    iucn = serializers.CharField(allow_blank=True, allow_null=True, )
    prioritaria = serializers.CharField(allow_blank=True, allow_null=True, max_length=200)
    exoticainvasora = serializers.CharField(allow_blank=True, allow_null=True, max_length=40)
    longitud = serializers.FloatField()
    latitud = serializers.FloatField()
    estadomapa = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    municipiomapa = serializers.CharField(allow_blank=True, allow_null=True, max_length=160)
    localidad = serializers.CharField(allow_blank=True, allow_null=True, max_length=510)
    fechacolecta = serializers.CharField(allow_blank=True, allow_null=True, max_length=20)
    anp = serializers.CharField(allow_blank=True, allow_null=True, max_length=500)
    probablelocnodecampo = serializers.CharField(allow_blank=True, allow_null=True, max_length=4)
    categoriaresidenciaaves = serializers.CharField(allow_blank=True, allow_null=True, max_length=200)
    formadecrecimiento = serializers.CharField(allow_blank=True, allow_null=True, max_length=200)
    fuente = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    taxonextinto = serializers.CharField(allow_blank=True, allow_null=True, max_length=30)
    usvseriev = serializers.CharField(allow_blank=True, allow_null=True, max_length=200)
    urlejemplar = serializers.CharField(allow_blank=True, allow_null=True, max_length=200)
    idejemplar = serializers.CharField(allow_blank=True, allow_null=True, max_length=64)
    ultimafechaactualizacion = serializers.DateField(allow_null=True)
    idnombrecatvalido = serializers.CharField(allow_blank=True, allow_null=True, max_length=40)
    idnombrecat = serializers.CharField(allow_blank=True, allow_null=True, max_length=40)
    reino = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    phylumdivision = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    clase = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    orden = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    familia = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    genero = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    especie = serializers.CharField(allow_blank=True, allow_null=True, max_length=200)
    calificadordeterminacion = serializers.CharField(allow_blank=True, allow_null=True, max_length=200)
    categoriainfraespecie = serializers.CharField(allow_blank=True, allow_null=True, max_length=40)
    categoriainfraespecie2 = serializers.CharField(allow_blank=True, allow_null=True, max_length=40)
    autor = serializers.CharField(allow_blank=True, allow_null=True, max_length=400)
    estatustax = serializers.CharField(allow_blank=True, allow_null=True, max_length=40)
    reftax = serializers.CharField(allow_blank=True, allow_null=True, max_length=510)
    taxonvalidado = serializers.CharField(allow_blank=True, allow_null=True, max_length=4)
    reinovalido = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    phylumdivisionvalido = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    clasevalida = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    ordenvalido = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    categoriainfraespecievalida = serializers.CharField(allow_blank=True, allow_null=True, max_length=40)
    autorvalido = serializers.CharField(allow_blank=True, allow_null=True, max_length=400)
    reftaxvalido = serializers.CharField(allow_blank=True, allow_null=True, max_length=510)
    nombrecomun = serializers.CharField(allow_blank=True, allow_null=True, )
    endemismo = serializers.CharField(allow_blank=True, allow_null=True, max_length=30)
    nivelprioridad = serializers.CharField(allow_blank=True, allow_null=True, max_length=40)
    region = serializers.CharField(allow_blank=True, allow_null=True, max_length=300)
    datum = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    geovalidacion = serializers.CharField(allow_blank=True, allow_null=True, max_length=140)
    paismapa = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    claveestadomapa = serializers.CharField(allow_blank=True, allow_null=True, max_length=20)
    mt24claveestadomapa = serializers.CharField(allow_blank=True, allow_null=True, max_length=20)
    mt24nombreestadomapa = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    clavemunicipiomapa = serializers.CharField(allow_blank=True, allow_null=True, max_length=20)
    mt24clavemunicipiomapa = serializers.CharField(allow_blank=True, allow_null=True, max_length=20)
    mt24nombremunicipiomapa = serializers.CharField(allow_blank=True, allow_null=True, max_length=160)
    incertidumbrexy = serializers.IntegerField(allow_null=True)
    altitudmapa = serializers.IntegerField(allow_null=True)
    usvseriei = serializers.CharField(allow_blank=True, allow_null=True, max_length=200)
    usvserieii = serializers.CharField(allow_blank=True, allow_null=True, max_length=200)
    usvserieiii = serializers.CharField(allow_blank=True, allow_null=True, max_length=200)
    usvserieiv = serializers.CharField(allow_blank=True, allow_null=True, max_length=200)
    coleccion = serializers.CharField(allow_blank=True, allow_null=True, max_length=300)
    institucion = serializers.CharField(allow_blank=True, allow_null=True, max_length=510)
    paiscoleccion = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    numcatalogo = serializers.CharField(allow_blank=True, allow_null=True, max_length=200)
    numcolecta = serializers.CharField(allow_blank=True, allow_null=True, max_length=200)
    procedenciaejemplar = serializers.CharField(allow_blank=True, allow_null=True, max_length=40)
    determinador = serializers.CharField(allow_blank=True, allow_null=True, max_length=510)
    fechadeterminacion = serializers.CharField(allow_blank=True, allow_null=True, max_length=20)
    diadeterminacion = serializers.IntegerField(allow_null=True)
    mesdeterminacion = serializers.IntegerField(allow_null=True)
    aniodeterminacion = serializers.IntegerField(allow_null=True)
    colector = serializers.CharField(allow_blank=True, allow_null=True, max_length=510)
    diacolecta = serializers.IntegerField(allow_null=True)
    mescolecta = serializers.IntegerField(allow_null=True)
    aniocolecta = serializers.IntegerField(allow_null=True)
    tipo = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    ejemplarfosil = serializers.CharField(allow_blank=True, allow_null=True, max_length=30)
    proyecto = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)
    formadecitar = serializers.CharField(allow_blank=True, allow_null=True, max_length=1000)
    urlproyecto = serializers.CharField(allow_blank=True, allow_null=True, max_length=510)
    obsusoinfo = serializers.CharField(allow_blank=True, allow_null=True, max_length=510)
    version = serializers.CharField(allow_blank=True, allow_null=True, max_length=20)
    idestadomapa = serializers.IntegerField(allow_null=True)
    idmunicipiomapa = serializers.IntegerField(allow_null=True)
    mt24idestadomapa = serializers.IntegerField(allow_null=True)
    mt24idmunicipiomapa = serializers.IntegerField(allow_null=True)
    idanpfederal1 = serializers.IntegerField(allow_null=True)
    idanpfederal2 = serializers.IntegerField(allow_null=True)
    especievalidabusqueda = serializers.CharField(allow_blank=True, allow_null=True, max_length=200)
    comentarioscat = serializers.CharField(allow_blank=True, allow_null=True, max_length=510)
    comentarioscatvalido = serializers.CharField(allow_blank=True, allow_null=True, max_length=510)
    fuentevalidacionespecie = serializers.CharField(allow_blank=True, allow_null=True, max_length=510)
    homonimosgenero = serializers.CharField(allow_blank=True, allow_null=True, )
    homonimosespecie = serializers.CharField(allow_blank=True, allow_null=True, )
    homonimosinfraespecie = serializers.CharField(allow_blank=True, allow_null=True, )
    homonimosgenerocatvalido = serializers.CharField(allow_blank=True, allow_null=True, )
    homonimosespeciecatvalido = serializers.CharField(allow_blank=True, allow_null=True, )
    homonimosinfraespeciecatvalido = serializers.CharField(allow_blank=True, allow_null=True, )
    categoriataxonomica = serializers.CharField(allow_blank=True, allow_null=True, max_length=100)

    def create(self, validate_data):
        #Fill the no request fields of snib table
        validate_data['the_geom'] = GEOSGeometry('Point(' + str(validate_data['longitud']) + ' ' + str(validate_data['latitud']) + ')', srid=4326)
        first_intersection = Grid8KmAoi.objects.get(the_geom__intersects=validate_data['the_geom'])
        validate_data['gridid_64km'] = first_intersection.gridid_64km
        validate_data['gridid_32km'] = first_intersection.gridid_32km
        validate_data['gridid_16km'] = first_intersection.gridid_16km
        validate_data['gridid_8km'] = first_intersection.gridid_8km
        #SpSnib attributes
        spsnib_data = {
            'reinovalido':validate_data['reinovalido'],
            'phylumdivisionvalido':validate_data['phylumdivisionvalido'],
            'clasevalida':validate_data['clasevalida'],
            'ordenvalido':validate_data['ordenvalido'],
            'familiavalida':validate_data['familiavalida'],
            'generovalido':validate_data['generovalido'],
            'especievalidabusqueda':validate_data['especievalidabusqueda']
        }
        #IspSnib attributes
        ispsnib_data = {
            'reinovalido':validate_data['reinovalido'],
            'phylumdivisionvalido':validate_data['phylumdivisionvalido'],
            'clasevalida':validate_data['clasevalida'],
            'ordenvalido':validate_data['ordenvalido'],
            'familiavalida':validate_data['familiavalida'],
            'generovalido':validate_data['generovalido'],
            'especievalidabusqueda':validate_data['especievalidabusqueda'],
            'especievalida':validate_data['especievalida'],
            'categoriainfraespecievalida':validate_data['categoriainfraespecievalida']
        }

        #GeoSnib attributes
        geosnib_data = {
            'longitud':validate_data['longitud'],
            'latitud':validate_data['latitud']
        }
        #Insertions in tables or get the existing row
        ispsnib, inserted_isp = IspSnib.objects.get_or_create(**ispsnib_data)
        spsnib, inserted_sp = SpSnib.objects.get_or_create(**spsnib_data)
        geosnib, inserted_geo = GeoSnib.objects.get_or_create(**geosnib_data)
        #Add ids of different tables
        validate_data['spid'] = spsnib.spid
        validate_data['ispid'] = ispsnib.ispid
        validate_data['geoid'] = geosnib.geoid
        #Create snib row
        snib_inserted = Snib.objects.create(**validate_data)
        #Fill the no request fields of sp_snib table
        if inserted_sp:
            spsnib.cells_64km = [first_intersection.gridid_64km]
            spsnib.cells_32km = [first_intersection.gridid_32km]
            spsnib.cells_16km = [first_intersection.gridid_16km]
            spsnib.cells_8km  = [first_intersection.gridid_8km]
            logger.info("Validando ({0}, {1}, {2})".format(validate_data["especievalidabusqueda"], validate_data["phylumdivisionvalido"], validate_data["reinovalido"]))
            validation_num = validate(snib_inserted)
            spsnib.validadoterceros = validation_num
            spsnib.save()
            logger.info('SpSnib inserted !!')
        else :
            spsnib.cells_64km.append(first_intersection.gridid_64km)
            spsnib.cells_64km = list(set(spsnib.cells_64km))
            spsnib.cells_32km.append(first_intersection.gridid_32km)
            spsnib.cells_32km = list(set(spsnib.cells_32km))
            spsnib.cells_16km.append(first_intersection.gridid_16km)
            spsnib.cells_16km = list(set(spsnib.cells_16km))
            spsnib.cells_8km.append(first_intersection.gridid_8km)
            spsnib.cells_8km  = list(set(spsnib.cells_8km))
            if spsnib.validadoterceros == None or spsnib.validadoterceros == 0:
                logger.info("Validando ({0}, {1}, {2})".format(validate_data["especievalidabusqueda"], validate_data["phylumdivisionvalido"], validate_data["reinovalido"]))
                validation_num = validate(snib_inserted)
                spsnib.validadoterceros = validation_num
            spsnib.save()
            logger.info('SpSnib grid lists modified !!')
        #Fill the no request fields of isp_snib table
        if inserted_isp:
            ispsnib.spid = spsnib.spid
            ispsnib.cells_64km = [first_intersection.gridid_64km]
            ispsnib.cells_32km = [first_intersection.gridid_32km]
            ispsnib.cells_16km = [first_intersection.gridid_16km]
            ispsnib.cells_8km  = [first_intersection.gridid_8km]
            ispsnib.save()
            logger.info('IspSnib inserted !!')
        else :
            ispsnib.cells_64km.append(first_intersection.gridid_64km)
            ispsnib.cells_64km = list(set(ispsnib.cells_64km))
            ispsnib.cells_32km.append(first_intersection.gridid_32km)
            ispsnib.cells_32km = list(set(ispsnib.cells_32km))
            ispsnib.cells_16km.append(first_intersection.gridid_16km)
            ispsnib.cells_16km = list(set(ispsnib.cells_16km))
            ispsnib.cells_8km.append(first_intersection.gridid_8km)
            ispsnib.cells_8km = list(set(ispsnib.cells_8km))
            ispsnib.save()
            logger.info('IspSnib grid lists modified !!')
        #Fill the no request fields of geo_snib table
        if inserted_geo:
            point900913 = GEOSGeometry('Point(' + str(validate_data['longitud']) + ' ' + str(validate_data['latitud']) + ')', srid=4326)
            point900913.transform(900913)
            validate_data['geom_m'] = point900913
            geosnib.the_geom = GEOSGeometry('Point(' + str(validate_data['longitud']) + ' ' + str(validate_data['latitud']) + ')', srid=4326)
            geosnib.gridid_64km = first_intersection.gridid_64km
            geosnib.gridid_32km = first_intersection.gridid_32km
            geosnib.gridid_16km = first_intersection.gridid_16km
            geosnib.gridid_8km  = first_intersection.gridid_8km
            geosnib.save()
            logger.info('GeoSnib inserted !!')
        # Add the spid to the corresponding kingdom in all the grids
        with connections['niche2'].cursor() as cursor:
            sql = ("UPDATE grid_{size}km_aoi SET {reino} = ARRAY(SELECT DISTINCT UNNEST(ARRAY_APPEND({reino}, {spid}))) WHERE gridid_{size}km = {gridid}")
            for size in ["8", "16", "32", "64"]:
                cursor.execute(sql.format(size=size, reino=spsnib.reinovalido.lower(), spid=spsnib.spid, gridid=getattr(geosnib, "gridid_{0}km".format(size))))
        return snib_inserted


class MultipleSnibSerializer(serializers.Serializer):
    data = SnibSerializer(many=True,
                          help_text = "Este atributo es una lista de JSON de tipo `snib` como en `insertsnib`")

    def create(self, validate_data):
        #save_multisnib.delay(validate_data)
        save_multisnib(validate_data)
        return object()


